io.stdout:setvbuf("no")

Gamestate = require "gamestate"

require "/states/menu/menu"
require "/states/menu/level_selection"
--require "/states/game/game"
require "/states/game/game2"

function love.load()
	love.window.setMode(1280, 720, {msaa = 0, resizable = false})

	Gamestate.registerEvents()
	Gamestate.switch(menu)
end


function love.update(dt)
	love.window.setTitle("Queue Madness (FPS:" .. love.timer.getFPS() .. ")")
end	