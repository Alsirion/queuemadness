Entity = {}

function Entity:new(curId, curName, curX, curY, actPnts)
  local obj = {}
  setmetatable(obj, self)
  self.__index = self
  obj.id = curId
  obj.name = curName
  obj.x = curX
  obj.y = curY
  obj.cx = curX
  obj.cy = curY
  obj.score = math.random(200)
  obj.curState = false
  obj.actPnts = actPnts
  obj.pickedPhrase = pickedPhrase
  obj.phraseDuration = phraseDuration

  return obj
end
--Old code
-- function Entity:move(size)
--   self.x = self.x + size
-- end
--End of old code


function Entity:moveB(step)
  self.id = self.id + step
  if self.id ~= nil and self.curState then
    if cells[self.id] == nil then
      self:changeState()
    else
      self.x = cells[self.id].x
      self.y = cells[self.id].y
      -- humanIsMoving = true
    end
  end
  self:changeAngle()
end

function Entity:move(dt)
  if humanIsMoving then
    
    if self.x ~= cells[self.id].x then
        self.x = self.x + 1
        self.y = self.y + 1
    else
      humanIsMoving = false
    end
  end
end

function Entity:say()
  self:say()
end
