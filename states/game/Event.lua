Event = {}

function Event:new(type)
  local obj = {}
  setmetatable(obj, self)
  self.__index = self
  self.type = type
  self.text = ""
  self.adding = 0
  -- self.tooltip = ""
  self.isActive = false
  return obj
end

-- --For skills tooltip only
-- function Event:_tostring()

--   return self.type .. "\n\r" .. self.tooltip

-- end

function Event:draw()
  if self.isActive and not isWin and not isLoose then
    love.graphics.setColor(100,100,100, 240)
    love.graphics.rectangle("fill", love.graphics.getWidth()/2-250, love.graphics.getHeight()/2-150, 500, 300)
    love.graphics.setColor(100,100,100, 255)
    love.graphics.rectangle("fill", love.graphics.getWidth()/2-200, love.graphics.getHeight()/2-125, 400, 250)
    love.graphics.setColor(200,100,100, 255)
    love.graphics.print(self.text, love.graphics.getWidth()/2-185, love.graphics.getHeight()/2-100)

    if self.type == "Buy" then
      if suit.Button("Buy", love.graphics.getWidth()/2-180, love.graphics.getHeight()/2, love.graphics.getWidth()/4, 25).hit then
        if p.score >= levelPrice then
          p.score = p.score - levelPrice
          game2:Win()
        else
          game2:Loose()
          LooseMessage = "У вас нехватило денег на нужную вещь. Приходите позднее"
        end
      end
      if suit.Button("Cancel", love.graphics.getWidth()/2 - 180, love.graphics.getHeight()/2+50, love.graphics.getWidth()/4, 25).hit then
        game2:Loose()
        LooseMessage = "Вы отказались купить то, за чем стояли в очереди."
      end
    end
  end
end

function Event:change(player)



  -- type = 1 - AddPoints
  -- type = 2 - Move Forward
  adding = 0
  if self.type == 1 then
      player.score = player.score + 1000
      self.text = "Богатеешь на глазах"
  elseif self.type == 2 then
--    print ("Попали в бросок вперед")
--    print (player.id)
    self.text = "Вы пытаетесь перепрыгнуть через 1 клетку,\nно встречаете там очередника.\nНачинается потасовка."
    -- local tempPlayerPoints = love.math.random(0, 10)
    local tempPlayerPoints = 10
    self.text = self.text.."\nВы бьёте на "..tostring(tempPlayerPoints)
    local tempHumanPoints = love.math.random(0, 10)
    self.text = self.text.."\nОн бьёт на "..tostring(tempHumanPoints)
    if tempPlayerPoints > tempHumanPoints then
      self.text = self.text.. "\nВы победили!\nИ перемещаетесь на его место."
      if player.id + 2 < levelSize then
--        p:moveB(1)
--        p:moveB(1)
        self.adding = 2
      elseif player.id + 1 < levelSize then
--        p:moveB(1)
        self.adding = 1
      else

      end
    else
      self.text = self.text.. "\nВы проиграли."
      self.adding = 0
    end
  elseif self.type == 3 then
    if math.random(100) >= 90 then
      self.text = "Вас выгнали из очереди"
      game2:Loose()
    else
      self.text = "Вас попытались выгнать из очереди, но не удалось"
    end
  return self.adding
  end


  --Skills definition

  --First skill - try to rob a human in front of you
  if self.type == "Rob" then
    for k,v in pairs(Humans) do
      if v.id == player.id + 1 then
        if math.random(100) >= 40 and v.score > 0 then
          player.score = player.score + v.score
          self.text = "Вы украли у впереди-стоящего " .. v.score .. " рублей"
          v.score = 0
          p.actPnts = p.actPnts -4
        elseif v.score <= 0 then
          self.text = "У впереди-стоящего больше нечего красть"
        else
          self.text = "Вы попытались украсть деньги у впереди-стоящего,\nно вам это не удалось"
          p.actPnts = p.actPnts -4
        end
      end
    end 
  end

  --Second skill - try to persuade human in front of you for leaving queue
  if self.type == "Persuade" then
    for k,v in pairs(Humans) do
      if v.id == player.id + 1 then
        if math.random(100) >= 40 + player.talkBonus then
          self.text = "Вам удалось уговорить человека выйти из очереди"
          v:changeState()
          -- p:moveB()
          self.adding = 1
          p.actPnts = p.actPnts -4
        else
          self.text = "Вам не удалось уговорить человека выйти из очереди"
          p.actPnts = p.actPnts -4
        end
        v:changeState()



      end
    end
  end


  --Technical events
  if self.type == "Break" then
    self.text = "У раздающих обед"
    isBreak = true
  elseif self.type == "EndBreak" then
    self.text = "Обед кончился"
    isBreak = false
  end
  if self.type == "Buy" then
    -- self.text = "asdasdasdasd"
    -- game2:Win()
    for k,v in pairs(items) do
      if v.keyword == levelReward then
        self.text = "Вы хотите купить " .. v.title .. " за " .. levelPrice .. " рублей?"
      end
    end
  end

    
end