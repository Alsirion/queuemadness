require "/states/game/Entity"



Human = {}
setmetatable(Human, Entity)

HumansName = {"Vasya", "Michael", "Adam", "Bill", "Mark", "Lucas", "George", "Milena", "Dianne", "Meredith", "Lara", "Karen", "Roberth", "Barney", "Lily", "Marshall", "Ted", "Neil", "Erick", "Linus", "Loretta", "Leonard", "Sheldon",
              "Howard", "Alexander" ,"Sophia", "Jackson", "Emma", "Aiden", "Olivia", "Lucas", "Ava", "Liam", "Mia", "Noah", "Isabella", "Ethan", "Riley", "Mason", "Aria", "Caden", "Zoe", "Oliver", "Charlotte", "Grayson", "Layla",
               "Jacob", "Amelia", "Michael", "Emily", "Benjamin", "Madelyn", "Carter", "Felix"}
HumansLastName = {"Gusev", "Hiberth", "Cumberbatch", "Bombercatch", "Volochai", "Newton", "Calderone", "Truestory", "Goldberg", "Stinson", "Aldrin", "Eriksen", "Mosby", "Anderson", "DeGrass-Tyson", "Hofstadter",
                  "Cooper", "Wolowitz", "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor", "Thomas", "Jackson", "White", "Harris", "Kjellberg"}

Phrases = {"Hi", "Sup", "How it's going?", "Fuck! I forgot to turn off the stove!", "TestPhrase? Really? Think about something more intrest", "I love the developers :)", "*with annoyance* Will this queue finish?", 
            "*with annoyance* Oooh, c'mon, move on", "Let's go, don't stand still!", "I wish I could go home...", "WAZAAP, old meme, actually...", "This is booooring"}
rainPhrases = {"Rain starts", "I forgot my umbrella", "Is it the rain?", "I hate drizzles", "Oh, great.", "Fuck rains..."}
stormPhrases = {"HOLY, what was that?", "IMPEDING DOOM APPROACHES!! Oh, that was just a thunder", "*scares of lightning*", "Heavy rain is terrible", "I'm soaking wet!"}
stoneAgePhrases = {"Arghh!", "Mine want chew!", "*roars*", "Exchange rate of stone is lowering *with sadness*", "Caves becoming more expensive *with sadness*", "Yum!", "*scratches*", "Sticks and boulders...", "My tamed bear is hungry!"
                    , "I need to clean up my cave!", "*angry* I want to eat someone!", "Yaargh!", "*with wonder* Intresting, will the people create a game about queues?", "Support that on stonestarter and branchlight",
                     "I saw a squirell runs for big acorn", "I want to check-in here in Stonesquare"}

finishPhrases = {"Finally!", "Hooray!"}

spriteArray = love.filesystem.getDirectoryItems("/images/levels/1/npc/")
reserveSpriteArray = {}


function Human:new(curId, curName, curX, curY, sprite, angle)
  local obj = Entity:new(curId, curName, curX, curY)
  setmetatable(obj, self)
  self.__index = self
  obj.r = love.math.random(240,255)
  obj.g = love.math.random(240,255)
  obj.b = love.math.random(240,255)

  obj.lastName = HumansLastName[love.math.random(#HumansLastName)]

  obj.angle = angle

  obj.sprite = sprite
  
  return obj
end

function Human:__tostring()
  return "Name: "..self.name.. "\n\rLast Name: "..self.lastName.."\n\rx: "..self.x.."\n\ry: "..self.y.."\n\rscore: "..self.score.."\n\rid: "..self.id
          .."\n\rcurState: "..tostring(self.curState)
          -- .."\n\r------\n\r"
end

function Human:tooltip()
  return "Name: "..self.name.."\nLast Name: "..self.lastName.."\nx: "..self.x.."\ny: "..self.y.."\nscore: "..self.score.."\nid: "..self.id
end

function Human:draw()
  love.graphics.setColor(self.r, self.g, self.b)
  love.graphics.draw(self.sprite, self.cx + 16, self.cy + 16, self.angle, 1, 1, self.sprite:getWidth()/2, self.sprite:getHeight()/2)

end

function Human:changeState()

  self.curState = not self.curState
end

function Human:generate(length, cells)

  local opponents = {}
  for i=1, length do
    -- if cells[i+3] == nil and not cells[i+2] == nil  then
    --   angle = -math.atan2(cells[i+1].x - cells[i+2].x, cells[i+1].y - cells[i+2].y)
    -- elseif cells[i+2] == nil then
    --   angle = math.rad(90)
    -- elseif cells[i+3] ~= nil then
    --   angle = -math.atan2(cells[i+1].x - (cells[i+2].x + cells[i+3].x)/2, cells[i+1].y - (cells[i+2].y + cells[i+3].y)/2)
    -- end
    opponents[i] = Human:new(i+1, HumansName[love.math.random(#HumansName)], cells[i+1].x, cells[i+1].y, Human:pickRandomSprite(), 0)


  end

  return opponents
end

function Human:say(weather, epoch)
  phrasePool = Phrases
  
  if weather == "rain" then    
    for k,v in pairs(rainPhrases) do
      table.insert(phrasePool, v)
    end
  elseif weather == "thunder" then
    
  elseif weather == "snow" then
  end

  if epoch == "Stone age" then    
    for k,v in pairs(stoneAgePhrases) do
      table.insert(phrasePool, v)
    end
  end

  if self.curState == "False" then
    for k,v in pairs(finishPhrases) do
      table.insert(phrasePool, v)
    end
  end

  pickedPhrase = phrasePool[math.random(#phrasePool)]
  return pickedPhrase  
end

function Human:move()
  -- self.id = self.id + 1
  -- self.curState = not self.curState

end

function Human:pickRandomSprite()
  local sprite = nil
  -- if epoch == "Stone age" then  
    if #spriteArray > 0 then
      spriteNumber = love.math.random(#spriteArray)
      sprite = love.graphics.newImage("/images/levels/1/npc/" .. spriteArray[spriteNumber])
      table.insert(reserveSpriteArray, spriteArray[spriteNumber])
      table.remove(spriteArray, spriteNumber)

    else
      spriteArray = reserveSpriteArray
      reserveSpriteArray = {}
      spriteNumber = love.math.random(#spriteArray)
      sprite = love.graphics.newImage("/images/levels/1/npc/" .. spriteArray[spriteNumber])
      table.insert(reserveSpriteArray, spriteArray[spriteNumber])
      table.remove(spriteArray, spriteNumber)

    end
    return sprite
  -- end

  
end

function Human:changeAngle()
  if cells[self.id+3] ~= nil then
    self.angle = -math.atan2(cells[self.id].x - (cells[self.id+1].x + cells[self.id+2].x)/2, cells[self.id].y - (cells[self.id+1].y + cells[self.id+2].y)/2)
    
  elseif cells[self.id+2] == nil then
    self.angle = math.rad(90)
  elseif cells[self.id+2] == nil and not cells[self.id+1] == nil  then
    self.angle = -math.atan2(cells[self.id].x - cells[self.id+1].x, cells[self.id].y - cells[self.id+1].y)
  end

end

function Human:destructor()
--  for k, v in ipairs(self) do
--    if v == self then
--      table.remove(self, k)
--      self = nil
--      return
--    end
--  end
  table.remove(Humans, self.id)
--  Humans[self.id] = nil
end