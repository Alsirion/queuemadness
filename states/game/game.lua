require "/states/game/Player"
require "/states/game/Human"
require "/states/game/MapGenerator/MapGenerator"
require "/states/game/MapGenerator/Cell"
require "/states/game/Event"
require "/states/game/WeatherSystem"
local LightWorld = require "lib"



game = {}
local timer = 0
local phraseTimer = 0
local timeOfDay = nil

lightMouseStrength = 0

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
          x2 < x1+w1 and
          y1 < y2+h2 and
          y2 < y1+h1
end



function game:init()
  love.graphics.setColor(100,100,200)
  love.graphics.setFont(mainFont)
  mainFont = love.graphics.newFont("/fonts/MyriadPro.ttf", 20)
  ui = love.graphics.newImage("/images/levels/1/ui/ui2.png")
  actPntsSprite = love.graphics.newImage("images/ui_pref/skill_point_active.png")
  pauseBtn = love.graphics.newImage("images/levels/1/ui/menu_button.png")
  pauseBtnActive = love.graphics.newImage("images/levels/1/ui/menu_button_active.png")

end

function game:enter()

  --Daylight
  if timeHrs >= 10 and timeHrs < 18 then
    ambientR = 245
    ambientG = 245
    ambientB = 245
    timeOfDay = "Day"
    --Evening
  elseif timeHrs >= 18 and timeHrs < 21 then
    ambientR = 170
    ambientG = 155
    ambientB = 155
    timeOfDay = "Evening"
    --Nighttime
  elseif timeHrs >= 21 or timeHrs < 5 then
    ambientR = 40
    ambientG = 40
    ambientB = 40
    timeOfDay = "Night"
    --Early Morning
  elseif timeHrs >= 5 and timeHrs < 10 then
    ambientR = 150
    ambientG = 150
    ambientB = 150
    timeOfDay = "Morning"
  end

  if timeOfDay ~= "Day" and levelEpoch == "Stone age" then
    lightMouseStrength = 150
  else
    lightMouseStrength = 0
  end


  lightWorld = LightWorld({

    ambient = {ambientR,ambientG,ambientB},
    refractionStrength = 32.0,
    reflectionVisibility = 0.75,
    --the general ambient light in the environment
  })

  lightMouse = lightWorld:newLight(0, 0, 200, 200, 100, lightMouseStrength)
  lightMouse:setGlowSize(0)










  currentStep = 1
  LastNum = 16
  --You can only use "smooth" and "random" arguments for different map generation patterns by now
  map = MapGen:new("smooth", levelSize, levelEpoch)
  cells = map:generate()
  p = Player:new(1, "FirstPlayer", map.startX, map.startY, 8)
  Humans = Human:generate(levelSize -1, cells)
  myEvent = Event:new(love.math.random(1,2))
  wType = "clear"

  skills = {}

  isPaused = false
  isWin = false

  duration = 0
  humanIsMoving = false

  --EndScreen variables
  startScore = 0

  timeSpentMts = 0
  timeSpentSds = 0
  timeMts = 0
  timeSds = 40
  timeZero = ""


  for _,v in pairs(Humans) do
    --change state to true (default false)
    v:changeState()
    v:changeAngle()
  end

  startScore = p.score
  if #campfireArray ~= 0 and timeOfDay ~= "Day" then
    for i = 0, #campfireArray, 5 do
      campfireArray[i+3] = lightWorld:newLight(campfireArray[i+1], campfireArray[i+2], 200, math.random(140, 150), 100, math.random(200, 300))
    end
  end


end

function game:draw()

  lightWorld:draw(function()
    love.graphics.setColor(255,255,255)
    map:draw(dt)

    love.graphics.draw(ui, 0, 0, 0, 1, 1)
    love.graphics.setColor(100,100,100)


    p:draw()
    for _,Hum in pairs(Humans) do
      if Hum.curState then
        Hum:draw()

      end

    end
    for _,Cel in pairs(cells) do
      Cel:draw()
    end

    love.graphics.setColor(255, 255, 255)

    if suit.ImageButton(pauseBtn, {hovered = pauseBtnActive}, love.graphics.getWidth() - 58, 12).hit and not isWin then
      isPaused = not isPaused
    end

    WeatherSys:draw(wType)

  end)
  myEvent:draw()

  if duration > 0 then

    love.graphics.setColor(100, 100, 100, 100)
    love.graphics.rectangle("fill", curHum.x + phraseX - 10, curHum.y + phraseY - 10, mainFont:getWidth(humPhrase) + 20, 45)
    love.graphics.setColor(255, 255, 255)
    love.graphics.print(humPhrase, curHum.x + phraseX, curHum.y + phraseY)

    if not isPaused then
      duration = duration -1
    end
  end


  love.graphics.print(p.score, 35, 37)
  love.graphics.print(timeMts .. " : " .. timeZero .. timeSds, love.graphics.getWidth()/2 - #(timeMts .. " : " .. timeZero .. timeSds) * 4, 8)

  for i = 0, p.actPnts-1 do
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(actPntsSprite, i*26 + 12, 8)
    love.graphics.setColor(100, 100, 200)
    -- love.graphics.circle("fill", i * 30, 25, 12, 12)
  end

  if isPaused then
    pauseMenu()
  end

  suit.draw()
  if isWin then
    timeSpentMts = math.floor(timeSpentSds/60)
    timeSpentSds = timeSpentSds%60


    love.graphics.setColor(100, 100, 100, 100)
    love.graphics.rectangle("fill", 200, 200, love.graphics.getWidth() - 400, love.graphics.getHeight() - 400)
    love.graphics.setColor(255, 255, 255)



    love.graphics.setColor(255, 255, 255)
    love.graphics.print("Gained money: " .. p.score - startScore, love.graphics.getWidth()/3, love.graphics.getHeight()/3)
    love.graphics.print("Time spent: " .. timeSpentMts .. " minutes " .. timeSpentSds .. " seconds", love.graphics.getWidth()/3, love.graphics.getHeight()/3 + 100)
    love.graphics.print("Press spacebar to continue", love.graphics.getWidth()/2 - 50, love.graphics.getHeight()/3*2)


  end


end

function game:update(dt)

	map:update(dt)

  if not love.window.hasFocus() then
    isPaused = true
  end
  -- pauseButton = suit.Button("Pause", love.graphics.getWidth() - 60, 10, 50, 50)
  -- pauseButton.Image = (pauseImage)
  -- pauseButton.Image(pauseBtn)

  if not isPaused then

    timer = timer + love.timer.getDelta()
    phraseTimer = phraseTimer + love.timer.getDelta()

    --Fire flickering
    if #campfireArray ~= 0 and timeOfDay ~= "Day" then
      for i = 0, #campfireArray, 5 do
        if math.random(1,2) == 1 and campfireArray[i+3].range < 300 then
          campfireArray[i+3]:setRange(campfireArray[i+3].range + 2)
        elseif math.random(1,2) == 1 and campfireArray[i+3].range > 50 then
          campfireArray[i+3]:setRange(campfireArray[i+3].range - 2)
        end
      end
    end

    --Mouse light flickering
    if timeOfDay ~= "Day" then
      if math.random(1,2) == 1 and lightMouse.range < 200 then
        lightMouse:setRange(lightMouse.range + 1)
      elseif math.random(1,2) == 2 and lightMouse.range > 100 then
        lightMouse:setRange(lightMouse.range - 1)
      end
    end



    lightMouse:setPosition(love.mouse.getX(), love.mouse.getY())


    if timer >= 1 and not isWin and not myEvent.isActive then
      if timeSds <= 0 and timeMts == 0 then
        turnEnd()
      else
        timeSds = timeSds -1
        if timeSds < 10 then
          timeZero = 0
        elseif timeSds > 10 or timeSds <= 0 then
          timeZero = ""
        end

        if timeSds == 0 and timeMts > 0 then
          timeSds = 60
          timeMts = timeMts - 1
        end

      end
      timer = 0
    end

    if p.actPnts <= 0 then
      turnEnd()
    end


    for i = 0, 7 do
      skills[i] = suit.Button("Skill" .. i, 150 + i * 75, love.graphics.getHeight() - 100, 75, 75)
    end


    if skills[0].hit and p.actPnts >= 4 then

      myEvent.isActive = not myEvent.isActive
      if myEvent.isActive then
        myEvent.type = "Rob"
        myEvent:change(p)
      end

    elseif skills[0].hit and p.actPnts < 4 then

    end

    if phraseTimer >= 5 then

      if math.random(3) >= 2 then
        -- if true then
        curHum = Humans[math.random(#Humans)]
        if curHum.curState then
          humPhrase = curHum:say(wType, levelEpoch)
          phraseX = math.random(-20, 20)
          phraseY = math.random(-30, -35)
          duration = 500
        end
      end
      phraseTimer = 0
    end
  else

    if suit.Button("Resume", love.graphics.getWidth()/2 - love.graphics.getWidth()/8, love.graphics.getHeight()/4, love.graphics.getWidth()/4, 75).hit then
      isPaused = false
    end
    if suit.Button("Settings", love.graphics.getWidth()/2 - love.graphics.getWidth()/8, love.graphics.getHeight()/4*2, love.graphics.getWidth()/4, 75).hit then

    end
    if suit.Button("Exit", love.graphics.getWidth()/2 - love.graphics.getWidth()/8, love.graphics.getHeight()/4*3, love.graphics.getWidth()/4, 75).hit then
      Gamestate.switch(level_sel)
    end

  end
end

function turnEnd()
  if p.id >= levelSize or myEvent.isActive then
    return
  end

  if love.math.random(0,1) == 1 then
    -- print("Попали в смену состояния")

    myEvent.isActive = not myEvent.isActive

    if myEvent.isActive then
      myEvent.type = love.math.random(1,2)
      myEvent:change(p)
    end
  end
  currentStep = currentStep + 1

  p:moveB()

  for _,Hum in pairs(Humans) do
    if Hum.id + 1 == p.id then
      -- print ("Current player.id: "..p.id)
      -- print ("Current human.id: "..Hum.id)
      Hum.curState = false
    end
    if (Hum.curState) then
      Hum:moveB()
    end
  end

  if math.random(3) >= 3 then
    WeatherSys:chooseWeather()
  end
  p.actPnts = 8
  p.score = p.score + 10

  timeSpentSds = timeSpentSds + (40 - timeSds)
  timeSpentMts = timeSpentMts + timeMts

  timeSds = 40


  if p.id == map.length then
    game:Win()
  end

end

function game:keypressed(key)
  if not isPaused and not isWin then
    if key == "e" then
      if myEvent.isActive then
        myEvent.isActive = not myEvent.isActive
      end
      turnEnd()
    elseif key == "escape" then

      isPaused = not isPaused
    elseif key == "s" then
      p:save(currentStep)
      --DebugOption
    elseif key == "z" and not (p.actPnts <= 0) and not myEvent.isActive then
      p.actPnts = p.actPnts - 2
    elseif key == "t" then
      map = MapGen:new("smooth", levelSize)
    elseif key == "d" then
      myEvent.isActive = not myEvent.isActive
    elseif key == "w" then
      self:Win()
    end
  elseif isPaused then
    if key == "escape" then
      isPaused = false
    end
  end
  if isWin and key == "space" then
    for k in pairs(game) do
      game[k] = nil
    end
    Gamestate.switch(level_sel)
  end

end

function game:keyreleased(key)


end

function pauseMenu()
  love.graphics.setColor(100, 100, 100, 100)
  love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
end

function game:Win()
  isWin = true
  -- for k in pairs(self) do
  -- 	game[k] = nil
  -- end
end