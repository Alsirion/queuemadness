require "/states/game/Inventory/Item"

Inventory = {}



local bg = love.graphics.newImage("images/ui/inventory.png")

invExitBtn = love.graphics.newImage("images/ui/inventory_parts/exit/exit.png")
invExitBtnHover = love.graphics.newImage("images/ui/inventory_parts/exit/exit_hover.png")
invExitBtnPressed = love.graphics.newImage("images/ui/inventory_parts/exit/exit_pressed.png")

-- invMoveLeftBtn = love.graphics.newImage("images/ui/inventory_parts/arrow/left_arrow.png")
-- invMoveLeftBtnHover = love.graphics.newImage("images/ui/inventory_parts/arrow/left_arrow_hover.png")
-- invMoveLeftBtnPressed = love.graphics.newImage("images/ui/inventory_parts/arrow/left_arrow_pressed.png")


invMoveLeftBtn = love.graphics.newImage("images/ui/inventory_parts/arrow/arrow.png")
invMoveLeftBtnHover = love.graphics.newImage("images/ui/inventory_parts/arrow/arrow_hover.png")
invMoveLeftBtnPressed = love.graphics.newImage("images/ui/inventory_parts/arrow/arrow_pressed.png")

invMoveRightBtn = love.graphics.newImage("images/ui/inventory_parts/arrow/arrow.png")
invMoveRightBtnHover = love.graphics.newImage("images/ui/inventory_parts/arrow/arrow_hover.png")
invMoveRightBtnPressed = love.graphics.newImage("images/ui/inventory_parts/arrow/arrow_pressed.png")


local itemXOffset = 0
local itemYOffset = 151

local isSelected = false

local dynamicItemsList = {}

local iter = 1
local xBackup = 0
local yBackup = 0

invGUI = suit:new()

function Inventory:new()
  local obj = {}
  setmetatable(obj, self)
  self.__index = self


  return obj
end

function Inventory:draw()
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(bg, 0, 0)
	
	invGUI:draw()

	if invGUI:ImageButton(invExitBtn, {hovered = invExitBtnHover, active = invExitBtnPressed}, 936, 142).hit then
		inventoryOpen = false
	end

	if invGUI:ImageButton(invMoveLeftBtn, {hovered = invMoveLeftBtnHover, active = invMoveLeftBtnPressed}, 936, 394).hit then
	end
	if invGUI:ImageButton(invMoveRightBtn, {hovered = invMoveRightBtnHover, active = invMoveRightBtnPressed}, 936, 412).hit then
	end

	-- love.graphics.print(tostring(self.mousePressed))



	if #p.items > 0 then

		iter = 1
		for k,v in pairs(p.items) do
			xBackup = itemXOffset
			yBackup = itemYOffset

			if not v.isWeared then
				-- iter = 1
				itemXOffset = 355 + iter * 72
				if math.fmod(iter, 8) == 0 then
				-- if itemXOffset > 670 then
					itemXOffset = 427
					itemYOffset = itemYOffset + 72

				end

				-- love.graphics.rectangle("line", itemXOffset, itemYOffset, v.sprite:getWidth(), v.sprite:getHeight())
				love.graphics.draw(v.sprite, itemXOffset, itemYOffset)
			else
				if v.bodyPart == "hand1" then

					itemXOffset = 339
					itemYOffset = 439
					
					love.graphics.draw(v.sprite, itemXOffset, itemYOffset )

				end
				iter = iter - 1
			end

			if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      itemXOffset, itemYOffset, v.sprite:getWidth(), v.sprite:getHeight()) and not isPaused then

				isSelected = true

				love.graphics.rectangle("line", itemXOffset, itemYOffset, v.sprite:getWidth(), v.sprite:getHeight())
				-- invGUI:Label(v.title .. "\n\n" .. v.message, 435, 444, 470, 10)
				-- self.textbox.text = v.title .. "\n\n" .. v.message
				-- self.mousePressed = not self.mousePressed
				
				if v.isWearable and love.mouse.isDown("2") then

					if v.isWeared then 
						v.isWeared = false
						
						p.stealBonus = 0
						p.fightBonus = 0
						p.talkBonus = 0
						p.moneyBonus = 0
						
					elseif not v.isWeared then 
						v.isWeared = true

						p.stealBonus = v.stealBonus
						p.fightBonus = v.fightBonus
						p.talkBonus = v.talkBonus
						p.moneyBonus = v.moneyBonus
					end
					
				end

				
			end

			itemXOffset = xBackup
			itemYOffset = yBackup
			iter = iter + 1

		end
		

	end

	if not isSelected and not isPaused then
		-- invGUI:Label("Steal bonus: " .. p.stealBonus .. "%\nDamage bonus: " .. p.fightBonus .. "%\nPersuade bonus: " .. p.talkBonus .. "%\nIncome bonus: " .. p.moneyBonus .. "%", 435, 444, 470, 10)
	end
	isSelected = false

	itemYOffset = 151

end









function Inventory:loadItems()

	-- Place for code of loading of items 
	-- Use contains = {}, Luke

end
