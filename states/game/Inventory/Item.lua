Item = {}


function Item:new(id)
  local obj = {}
  setmetatable(obj, self)
  self.__index = self

  self.id = id

  self.keyword = keyword
  self.title = ""
  self.message = ""

  self.isWearable = false
  self.isWeared = false
  self.bodyPart = nil
  self.sprite = nil

  self.stealBonus = 0
  self.fightBonus = 0
  self.talkBonus = 0
  self.moneyBonus = 0

  return obj
end

function Item:generate()

	if self.id == 1 then
		self.title = "Spark of eternal fire"
		self.message = "This is the spark of first fire, ever created by human. Pretty warm, though."
		self.keyword = "spark"

		self.sprite = love.graphics.newImage("images/sprites/items/fire.png")
	elseif self.id == 2 then
		self.title = "Square wheel"
		self.message = "Very first wheel, created by the mankind. Yes, it was hard to obtain something circle shaped back then."
		self.keyword = "wheel"

		self.sprite = love.graphics.newImage("images/sprites/items/wheel.png")
	elseif self.id == 3 then
		self.title = "Last letter of the alphabet"
		self.message = "Did you knew, what very first alphabet was created about XIX century BC by Egyptians? Last letter of it was \"taw\", which was represented by X-shaped sign."
		self.keyword = "letter"

		self.sprite = love.graphics.newImage("images/sprites/items/abc.png")
	elseif self.id == 4 then
		self.title = "List of first paper"
		self.message = "First paper was created by Chinese and was made out of cotton in 105 year AD. After almost 2 thousands of years it looks like stone, but who cares?"
		self.keyword = "paper"

		self.sprite = love.graphics.newImage("images/sprites/items/paper.png")
	elseif self.id == 5 then
		self.title = "Bowstring of the first bow"
		self.message = "Bows existing so long in the mankind history, so none can tell, when was first bowstring one created. We think, it was made out of wines or softered bark."
		self.keyword = "bowstring"

		self.sprite = love.graphics.newImage("images/sprites/items/string.png")
	elseif self.id == 6 then
		self.title = "Very first axe"
		self.message = "Very first stick with very first stone. Enjoy. Using increases your persuade chance to +20%. \nRMB - wear/pull off"
		self.keyword = "axe"

		self.isWearable = true
		self.bodyPart = "hand1"
		self.talkBonus = 20

		self.sprite = love.graphics.newImage("images/sprites/items/axe.png")
	elseif self.id == 7 then
		self.title = "First face powder"
		self.message = "Was actually used by represantives of very first profession. CAUTION! Highly explosive!"
		self.keyword = "powder"

		self.sprite = love.graphics.newImage("images/sprites/items/puder.png")
	elseif self.id == 8 then
		self.title = "Eye of the first needle"
		self.message = "Try to stick first string there, and you will make an ultimate artifact, which can do... Actually nothing, but you can to darn your sock."
		self.keyword = "needle_eye"

		self.sprite = love.graphics.newImage("images/sprites/items/needle.png")
	end

end


