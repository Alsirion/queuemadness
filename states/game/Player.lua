local filename = "save.txt"
local angle = math.rad(90)
require "/states/game/Entity"

Player = {}
setmetatable(Player, Entity)

playerItems = {}

function Player:new(curId, curName, curX, curY, actPnts)
  local obj = Entity:new(curId, curName, curX, curY, actPnts)
  setmetatable(obj, self)
  self.__index = self
  self.image = love.graphics.newImage("/images/player/player.png")
  self.angle = angle

  self.items = playerItems

  self.stealBonus = 0
  self.fightBonus = 0
  self.talkBonus = 0
  self.moneyBonus = 0
  return obj
end

function Player:__tostring()
  return "name: "..self.name.."\n\rx: "..self.x.."\n\ry: "..self.y.."\n\rscore: "..self.score.."\n\rAction Points: "..self.actPnts.."\n\rBonus to stealing: "..self.stealBonus.."\n\rBonus to fighting: "..self.fightBonus
  .."\n\rBonus to money: "..self.moneyBonus
end


--function Player:update(dt)
--
--end

function Player:draw()
  -- self.angle = -math.atan2(love.mouse.getY() - self.y + 16, love.mouse.getX() - self.x + 16)
--  love.graphics.rectangle("fill", self.x, self.y, 32, 32)
  love.graphics.setColor(255, 255, 255)
  -- love.graphics.circle("fill", self.x + 16, self.y + 16, 32)
  love.graphics.draw(self.image, self.cx + 16, self.cy + 16, self.angle, 1, 1, self.image:getWidth()/2, self.image:getHeight()/2)
end

function Player:moveB(step)
  self.id = self.id + step
  self.x = cells[self.id].x
  self.y = cells[self.id].y
  self:changeAngle()
end

function Player:save(currentStep)
  local save = self.score..";"
  if love.filesystem.exists(filename) then
    love.filesystem.remove(filename)
  end
  love.filesystem.append(filename, save)
end

function Player:load()
  contents, size = love.filesystem.read(filename, bytes)
  local data = {}
  local i = 1
  for k in string.gmatch(contents, "(%w+);") do
    data[i] = k
    i = i + 1
  end
  -- self.name = data[1]
  self.score = tonumber(data[1])
  -- self.x = tonumber(data[3])
  -- currentStep = tonumber(data[4])
  -- self.actPnts = tonumber(data[5])
end

function Player:changeAngle()

  if cells[self.id+2] == nil and not cells[self.id+1] == nil  then
    self.angle = -math.atan2(cells[self.id].x - cells[self.id+1].x, cells[self.id].y - cells[self.id+1].y)

  elseif cells[self.id+3] ~= nil then
    self.angle = -math.atan2(cells[self.id].x - (cells[self.id+1].x + cells[self.id+2].x)/2, cells[self.id].y - (cells[self.id+1].y + cells[self.id+2].y)/2)
    elseif cells[self.id+2] == nil then
      self.angle = math.rad(90)
  end


end

