WeatherSys = {}

rainSheet = love.graphics.newImage("images/sprites/rain.png")

rainGrid = anim8.newGrid(1280, 720, rainSheet:getWidth(), rainSheet:getHeight())
rainAnim = anim8.newAnimation(rainGrid('1-8', 1), 0.05)

function WeatherSys:new()
  local obj = {}
  setmetatable(obj, self)
  self.__index = self


  return obj
end


function WeatherSys:chooseWeather()
  sw = love.math.random(0, 1)
  if sw == 0 then
    wType = "clear"
    return wType
  elseif sw == 1 then
    wType = "rain"
    return wType
  end
end

function WeatherSys:update(dt)
  rainAnim:update(dt)
end

function WeatherSys:draw(wType)
  if wType == "rain" then
    
    rainAnim:draw(rainSheet, 0, 0)
  end
end