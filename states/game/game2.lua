require "/states/game/Player"
require "/states/game/Human"
require "/states/game/MapGenerator/MapGenerator"
require "/states/game/MapGenerator/Cell"
require "/states/game/Event"
require "/states/game/WeatherSystem"
require "/states/game/Inventory/Inventory"
local LightWorld = require "lib"



game2 = {}
local timer = 0
local phraseTimer = 0
timeOfDay = nil
local globalTimer = 0
tooltipTimer = 0
isTooltipShowing = false

items = {}

local pauseGUI = suit:new()
mainGUI = suit:new()

inventoryOpen = false

lightMouseStrength = 0

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
          x2 < x1+w1 and
          y1 < y2+h2 and
          y2 < y1+h1
end

-- Waypoint-only for Sublime
local function init() end
-- End of waypoint

function game2:init()
  love.graphics.setColor(100,100,200)
  love.graphics.setFont(mainFont)
  mainFont = love.graphics.newFont("/fonts/Red October-Regular.ttf", 20)
  ui = love.graphics.newImage("/images/ui/hud.png")
  actPntsSprite = love.graphics.newImage("images/ui/skill_points/skill_point_active.png")

  pauseBtn = love.graphics.newImage("images/ui/menu_button/menu_button.png")
  pauseBtnActive = love.graphics.newImage("images/ui/menu_button/menu_button_hover.png")
  pauseBtnPressed = love.graphics.newImage("images/ui/menu_button/menu_button_pressed.png")

  ultimateBtn = love.graphics.newImage("images/ui/sec_skill/sec_skill.png")
  ultimateBtnActive = love.graphics.newImage("images/ui/sec_skill/sec_skill_hover.png")
  ultimateBtnPressed = love.graphics.newImage("images/ui/sec_skill/sec_skill_pressed.png")

  inventoryBtn = love.graphics.newImage("images/ui/inventory_parts/button/button.png")
  inventoryBtnActive = love.graphics.newImage("images/ui/inventory_parts/button/button_hover.png")
  inventoryBtnPressed = love.graphics.newImage("images/ui/inventory_parts/button/button_pressed.png")

  
  
  


  skillsSprite = love.filesystem.getDirectoryItems("images/ui/skills")
  crateSprite = love.graphics.newImage("images/ui/hud_parts/crate/crate_null.png")
end

-- Waypoint-only for Sublime
local function enter() end
-- End of waypoint

function game2:enter()

  

  skillsArray = {}
  humanLights = {}

  for k, v in pairs(skillsSprite) do
    skillsArray[k] = love.graphics.newImage("images/ui/skills/" .. v) 
  end

  generateMap()
  generateLight()
  assignTooltips()

  generateInventory()
end

-- Waypoint-only for Sublime
local function draw() end
-- End of waypoint


function game2:draw()

  lightWorld:draw(function()
    love.graphics.setColor(255,255,255)
    map:draw(dt)

    love.graphics.setColor(200, 200, 200)
    WeatherSys:draw(wType)
    love.graphics.setColor(255, 255, 255)

    for _,Hum in pairs(Humans) do
      if Hum.cy < 850 then
        Hum:draw()
      else
      end
    end





    -- love.graphics.setColor(0, 255, 255)
    -- love.graphics.rectangle("line", 2, 655, 104, 30)
    -- love.graphics.setColor(0, 255, 0)
    -- love.graphics.rectangle("line", 2, 685, 104, 30)
    -- love.graphics.setColor(255, 255, 0)
    -- love.graphics.rectangle("line", 106, 655, 114, 30)
    -- love.graphics.setColor(255, 0, 0)
    -- love.graphics.rectangle("line", 106, 685, 114, 30)
    -- love.graphics.setColor(0, 0, 255)
    -- love.graphics.rectangle("line", 235, 655, 210, 30)
    -- love.graphics.setColor(255, 0, 255)
    -- love.graphics.rectangle("line", 235, 685, 210, 30)

    if mainGUI:ImageButton(ultimateBtn, {hovered = ultimateBtnActive, active = ultimateBtnPressed}, 1124, 656).hit and not (isWin or isLoose) then

    end
    if mainGUI:ImageButton(inventoryBtn, {hovered = inventoryBtnActive, active = inventoryBtnPressed}, 1052, 656).hit and not (isWin or isLoose) then
      inventoryOpen = not inventoryOpen
    end

    p:draw()

    for _,Cel in pairs(cells) do
      Cel:draw()
    end

    love.graphics.setColor(255, 255, 255)

    if mainGUI:ImageButton(pauseBtn, {hovered = pauseBtnActive, active = pauseBtnPressed}, 1208, 657).hit and not (isWin or isLoose) then
      isPaused = not isPaused
    end



  end)


  love.graphics.draw(ui, 0, 0, 0, 1, 1)
  -- love.graphics.setColor(100,100,100)

  if map.warehouseItemsCount == 0 then
    love.graphics.draw(crateSprite, 180, 660)
    love.graphics.draw(crateSprite, 149, 660)
    love.graphics.draw(crateSprite, 116, 660)
  elseif map.warehouseItemsCount < (map.allWarehouseItems/3) then
    love.graphics.draw(crateSprite, 180, 660)
    love.graphics.draw(crateSprite, 149, 660)
  elseif map.warehouseItemsCount < (map.allWarehouseItems/3*2) then     
    love.graphics.draw(crateSprite, 180, 660)
  end

  myEvent:draw()
  
  phraseCount()

  love.graphics.print(p.score, 262, 660)
  -- love.graphics.print(timeMts .. " : " .. timeZero .. timeSds, 38, 559)
  love.graphics.setColor(100, 200, 200)
  -- love.graphics.print(love.mouse.getX() .. " " .. love.mouse.getY(), 10, 10)
  -- love.graphics.print("[DEBUG] Items left: " .. map.warehouseItemsCount .. " [DEBUG]", 35, 75)
  love.graphics.print(globalTimeHrs .. " : " .. globalTimeZero  .. globalTimeMts, 38, 659)
  love.graphics.print(levelBreakHrsStart .. " : " .. levelBreakMtsStart, 38, 690)
  love.graphics.print(levelWorkHrsFinish .. " : " .. levelWorkMtsFinish .. "0", 148, 690)

  for i = 0, p.actPnts - 1 do
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(actPntsSprite, i*26 + 236, 690)
    -- love.graphics.setColor(100, 100, 200)
    -- love.graphics.circle("fill", i * 30, 25, 12, 12)
  end
  mainGUI:draw()
  
  if inventoryOpen then
    inv:draw()
  end
  if isPaused then
    pauseMenu()
  else
    checkMouseHover()
  end


  suit.draw()
  if isWin then
    timeSpentMts = math.floor(timeSpentSds/60)
    timeSpentSds = timeSpentSds%60


    love.graphics.setColor(100, 100, 100, 100)
    love.graphics.rectangle("fill", 200, 200, love.graphics.getWidth() - 400, love.graphics.getHeight() - 400)
    love.graphics.setColor(255, 255, 255)


    love.graphics.setColor(255, 255, 255)
    love.graphics.print("Gained money: " .. p.score - startScore, love.graphics.getWidth()/3, love.graphics.getHeight()/3)
    love.graphics.print("Time spent: " .. timeSpentMts .. " minutes " .. timeSpentSds .. " seconds", love.graphics.getWidth()/3, love.graphics.getHeight()/3 + 100)
    love.graphics.print("Press spacebar to continue", love.graphics.getWidth()/2 - 50, love.graphics.getHeight()/3*2)

  end
  if isLoose then
    timeSpentMts = math.floor(timeSpentSds/60)
    timeSpentSds = timeSpentSds%60


    love.graphics.setColor(100, 100, 100, 100)
    love.graphics.rectangle("fill", 200, 200, love.graphics.getWidth() - 400, love.graphics.getHeight() - 400)
    love.graphics.setColor(255, 255, 255)



    love.graphics.setColor(255, 255, 255)
    love.graphics.print(LooseMessage, love.graphics.getWidth()/3, love.graphics.getHeight()/3 - 25)
    love.graphics.print("Gained money: " .. p.score - startScore, love.graphics.getWidth()/3, love.graphics.getHeight()/3 + 75)
    love.graphics.print("Time spent: " .. timeSpentMts .. " minutes " .. timeSpentSds .. " seconds", love.graphics.getWidth()/3, love.graphics.getHeight()/3 + 100)
    love.graphics.print("Press spacebar to continue", love.graphics.getWidth()/2 - 50, love.graphics.getHeight()/3*2)
  end

end

-- Waypoint-only for Sublime
local function update() end
-- End of waypoint

function game2:update(dt)
  map:update(dt)

  WeatherSys:update(dt)

  for k,Hum in pairs(Humans) do
    

    if Hum.curState == false and Hum.y < 850 then
      Hum.y = Hum.y + 5
      Hum.angle = math.rad(180)
    end
    if Hum.cx < Hum.x then
      Hum.cx = Hum.cx + 2
    end
    if Hum.cy < Hum.y then
      Hum.cy = Hum.cy + 2
    end
    if Hum.cy > Hum.y then
      Hum.cy = Hum.cy - 2
    end
    humanLights[k]:setPosition(Hum.cx + 16, Hum.cy + 16)
  end
  if p.cx < p.x then
    p.cx = p.cx + 2
  end
-- <<<<<<< Updated upstream
  if p.cy < p.y then
    p.cy = p.cy + 2
  end
  if p.cy > p.y then
    p.cy = p.cy - 2
  end
-- =======
-- >>>>>>> Stashed changes

  playerLight:setPosition(p.cx + 16, p.cy + 16)

  if not love.window.hasFocus() and not isPaused then
    if gameDifficulty == "Easy" then

    elseif gameDifficulty == "Normal" then
      isPaused = true
    elseif gameDifficulty == "Hard" then
      self:Loose()
      LooseMessage = "Вы отвлеклись на что-то другое и вас вышвырнули из очереди."
    end
  end
  drawSkills()
  if not isPaused then

    countTime()
    flickLight()
    checkSkillPress()
    fireAnim:resume()

  else
    fireAnim:pause()

    if pauseGUI:Button("Resume", love.graphics.getWidth()/2 - love.graphics.getWidth()/8, love.graphics.getHeight()/4, love.graphics.getWidth()/4, 75).hit then
      isPaused = false
    end
    if pauseGUI:Button("Settings", love.graphics.getWidth()/2 - love.graphics.getWidth()/8, love.graphics.getHeight()/4*2, love.graphics.getWidth()/4, 75).hit then

    end
    if pauseGUI:Button("Exit", love.graphics.getWidth()/2 - love.graphics.getWidth()/8, love.graphics.getHeight()/4*3, love.graphics.getWidth()/4, 75).hit then
      Gamestate.switch(level_sel)
    end
  end





  -- if p.actPnts <= 0 then
  --   turnEnd2()
  -- end
end

-- Only for maths
function turnEnd2()
  --print ("TurnEnd")

  p.actPnts = 8

  timeSds = 0
  timeMts = 0

  map.warehouseItemsCount = map.warehouseItemsCount - 1
  if map.warehouseItemsCount == 0 then
    LooseMessage = "There are no more items left! Come again later."
    game2:Loose()
  end
  if p.id >= levelSizeString or myEvent.isActive then
    return
  end

  if love.math.random(100) <= 10 then
    -- print("Попали в смену состояния")

    -- myEvent.isActive = not myEvent.isActive

    -- if myEvent.isActive then
    --   myEvent.type = love.math.random(1,2)
    --   -- myEvent.type = 2
    --   myEvent:change(p)
    -- end
  end
  p:moveB(1 + myEvent.adding)
  print ("Player id: "..p.id)
  currentStep = currentStep + 1

  
--  Humans[levelSizeString-1] = nil
  for k, Hum in pairs (Humans) do
--    print (#Human)
    if Hum.id == levelSizeString or Hum.id + 1 == p.id then
      Hum.curState = false
     -- Humans[k] = nil
    else
      -- проверяем если, мы прыгаем и есть хуман который отстает от нас на 2, то двигаем их на 1 шаг вперед
      if myEvent.adding > 0 and  Hum.id + 2 < p.id then
        Hum:moveB(1)
      end
      Hum:moveB(1)
      print ("Human id: "..Hum.id)
    end
  end


  if math.random(3) >= 3 then
    WeatherSys:chooseWeather()
  end
  
  -- p.score = p.score + 10

  timeSpentSds = timeSpentSds + (40 - timeSds)
  timeSpentMts = timeSpentMts + timeMts

  timeSds = math.random(5, 15)
  if timeSds > 60 then
    timeMts = 1
    timeSds = timeSds - 60
  end


  if p.id == map.length then

    myEvent.isActive = not myEvent.isActive
    if myEvent.isActive then
      myEvent.type = "Buy"
      myEvent.change(p)
    end
  end
end

function game2:keypressed(key)
  if not isPaused and not (isWin or isLoose) then
    if key == "e" then
      -- if myEvent.isActive then
      --   myEvent.isActive = not myEvent.isActive
      -- end
      -- turnEnd2()
    elseif key == "escape" then

      isPaused = not isPaused
    elseif key == "s" then
      -- p:save(currentStep)
      --DebugOption
    elseif key == "z" and not (p.actPnts <= 0) and not myEvent.isActive then
      -- p.actPnts = p.actPnts - 2
    elseif key == "t" then
      -- map = MapGen:new("smooth", levelSizeString)
    elseif key == "d" then
      myEvent.isActive = not myEvent.isActive
    elseif key == "w" then

    -- myEvent.isActive = not myEvent.isActive
    -- if myEvent.isActive then
    --   myEvent.type = "Buy"
    --   myEvent:change(p)
    -- end

    elseif key == "b" then
      -- globalTimeMts = globalTimeMts + 10
    elseif key == "n" then
      -- globalTimeHrs = globalTimeHrs + 1
    elseif key == "c" then
      -- map.warehouseItemsCount = map.warehouseItemsCount + 1
    elseif key == "i" then
        -- for i = 1, 8 do
        --   p.items[i] = items[i]
        -- end
        -- for k,v in pairs(items) do
        --   p.items[k] = items[k]
        -- end
    end

  elseif isPaused then
    if key == "escape" then
      isPaused = false
    end
  end
  if (isWin or isLoose) and key == "space" then
--    for k in pairs(game) do
--      game[k] = nil
--    end
    Gamestate.switch(level_sel)
  end

end



function pauseMenu() 
  love.graphics.setColor(0, 0, 0, 180)
  love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
  love.graphics.setColor(255, 255, 255)

  love.graphics.print("How to play: \nJust wait until something happens.\nGood luck!", 900, 300)
  pauseGUI:draw()
end

function game2:Win()
  isWin = true
  for k,v in pairs(items) do
    
    if v.keyword == levelReward then
      table.insert(playerItems, v)
    end
  end

  table.insert(completedLevels, levelID) 

end

function game2:Loose()
  isLoose = true
end

function checkSkillPress()

  if skills[1].hit and p.actPnts >= 4 then

    myEvent.isActive = not myEvent.isActive
    if myEvent.isActive then
      myEvent.type = "Rob"
      myEvent:change(p)
    end

  elseif skills[1].hit and p.actPnts < 4 then
  end

  if skills[2].hit and p.actPnts >= 4 then

    myEvent.isActive = not myEvent.isActive
    if myEvent.isActive then
      myEvent.type = "Persuade"
      myEvent:change(p)
    end
  elseif skills[2].hit and p.actPnts < 4  then
  end

  if skills[3].hit and p.actPnts >= 4 then

    myEvent.isActive = not myEvent.isActive
    if myEvent.isActive then
      myEvent.type = 2
      myEvent:change(p)
    end
  elseif skills[3].hit and p.actPnts < 4  then
  end
end

function countTime()

  globalTimer = globalTimer + love.timer.getDelta()
  timer = timer + love.timer.getDelta()
  phraseTimer = phraseTimer + love.timer.getDelta()
  
  if not isTooltipShowing then
    tooltipTimer = 0
  end


  if timer >= 1 and not (isWin or isLoose or isBreak) and not myEvent.isActive then
    if timeSds <= 0 and timeMts == 0 then
      turnEnd2()
    else
      if timeSds == 0 and timeMts > 0 then
        timeSds = 60
        timeMts = timeMts - 1
      end
      timeSds = timeSds -1
    end
    timer = 0
  end



  if timeSds < 10 then
    timeZero = 0
  elseif timeSds > 10 or timeSds <= 0 then
    timeZero = ""
  end
  if globalTimeMts < 10 then
    globalTimeZero = 0
  else 
    globalTimeZero = ""
  end

  if phraseTimer >= 5 then

  if math.random(3) >= 2 then
    -- if true then
    curHum = Humans[math.random(#Humans)]
    if curHum.curState then
      humPhrase = curHum:say(wType, levelEpoch)
      phraseX = math.random(-20, 20)
      phraseY = math.random(-30, -35)
      duration = 500
    end
  end
  phraseTimer = 0
end

    

  if globalTimer >= 1 and not (isWin or isLoose) and not myEvent.isActive then

    globalTimeMts = globalTimeMts + 1

    if globalTimeMts >= 60 then
      if globalTimeHrs < 23 then
        generateLight()
        globalTimeHrs = globalTimeHrs + 1
      else
        globalTimeHrs = 0
      end
      globalTimeMts = 0
    end

    globalTimer = 0
  end



  if (globalTimeHrs >= levelBreakHrsStart and globalTimeHrs <= levelBreakHrsFinish) and (globalTimeMts >= levelBreakMtsStart and globalTimeMts <= levelBreakMtsFinish) and not isBreak then
    
    myEvent.isActive = not myEvent.isActive
    if myEvent.isActive then
      myEvent.type = "Break"
      myEvent:change(p)
    end
    
  elseif (globalTimeHrs >= levelBreakHrsFinish) and (globalTimeMts >= levelBreakMtsFinish) and isBreak then

    myEvent.isActive = not myEvent.isActive
    if myEvent.isActive then
      myEvent.type = "EndBreak"
      myEvent:change(p)
    end

  end

  if globalTimeHrs >= levelWorkHrsFinish and globalTimeMts >= levelWorkMtsFinish then
    LooseMessage = "Время работы очереди подошло к концу, приходите завтра"
    game2:Loose()

  end
end

function flickLight()

  --Fire flickering
  if #campfireArray ~= 0 and timeOfDay ~= "Day" then
    for i = 0, #campfireArray, 5 do
      if math.random(1,2) == 1 and campfireArray[i+3].range < 300 then
      -- if campfireArray[i+3].range < 300 then
        campfireArray[i+3]:setRange(campfireArray[i+3].range + 2)
      elseif math.random(1,2) == 1 and campfireArray[i+3].range > 50 then
      -- elseif campfireArray[i+3].range > 50 then
        campfireArray[i+3]:setRange(campfireArray[i+3].range - 2)
      end
    end
  end

  --Mouse light flickering
  if timeOfDay ~= "Day" then
    if math.random(1,2) == 1 and lightMouse.range < 200 then
      lightMouse:setRange(lightMouse.range + 1)
    elseif math.random(1,2) == 2 and lightMouse.range > 100 then
      lightMouse:setRange(lightMouse.range - 1)
    end
  end

  lightMouse:setPosition(love.mouse.getX(), love.mouse.getY())

end

function drawSkills()

  local spriteOffset = 464
  

  local iter = 1
  love.graphics.setColor(255,255,255)
  for i = 1, #skillsArray, 3 do
    
    skills[iter] = mainGUI:ImageButton(skillsArray[i], {hovered = skillsArray[i+1], active = skillsArray[i+2]}, spriteOffset, love.graphics.getHeight() - 63, 75, 75)

    spriteOffset = spriteOffset + 72
    iter = iter +1
  end

end

function phraseCount()

  if duration > 0 then
    -- if tooltipTimer >= delay then
    --   if love.mouse.getX() + mainFont:getWidth(message) + 10 < 1279 then
    --     love.graphics.setColor(100, 100, 100, 150)
    --     love.graphics.rectangle("fill", love.mouse.getX(), love.mouse.getY(), mainFont:getWidth(message) + 10, - 75)
    --     love.graphics.setColor(255, 255, 255)
    --     love.graphics.print(message, love.mouse.getX(), love.mouse.getY() - 75)
    --   else
    --     love.graphics.setColor(100, 100, 100, 150)
    --     love.graphics.rectangle("fill", love.mouse.getX() - (mainFont:getWidth(message) + 10), love.mouse.getY(), mainFont:getWidth(message) + 10, - 75)
    --     love.graphics.setColor(255, 255, 255)
    --     love.graphics.print(message, love.mouse.getX() - mainFont:getWidth(message) - 10, love.mouse.getY() - 75)
    --   end
    -- end
    if curHum.cx + phraseX - 10 + mainFont:getWidth(humPhrase) + 20 < 1279 then
      -- love.graphics.setColor(100, 100, 100, 100)
      -- love.graphics.rectangle("fill", curHum.cx + phraseX - 10, curHum.cy + phraseY - 10, mainFont:getWidth(humPhrase) + 20, 45)
      -- love.graphics.setColor(255, 255, 255)
      love.graphics.print(humPhrase, curHum.cx + phraseX, curHum.cy + phraseY)
    else
      -- love.graphics.setColor(100, 100, 100, 100)
      -- love.graphics.rectangle("fill", curHum.cx + phraseX - 10 - (mainFont:getWidth(humPhrase) + 20), curHum.cy + phraseY - 10, mainFont:getWidth(humPhrase) + 20, 45)
      -- love.graphics.setColor(255, 255, 255)
      love.graphics.print(humPhrase, curHum.cx + phraseX - (mainFont:getWidth(humPhrase) + 20), curHum.cy + phraseY)
    end
    if not isPaused then
      duration = duration -1
    end
  end

end

function generateMap()


  currentStep = 1
  LastNum = 16
  --You can only use "smooth" and "random" arguments for different map generation patterns by now
  map = MapGen:new("smooth", levelSizeString, levelEpoch, levelDifficulty)
  cells = map:generate()
  p = Player:new(1, "FirstPlayer", map.startX, map.startY, 8)
  Humans = Human:generate(levelSizeString -1, cells)
  myEvent = Event:new(love.math.random(1,2))
  wType = "clear"

  skills = {}

  isPaused = false
  isWin = false
  isLoose = false
  isBreak = false

  duration = 0

  humanIsMoving = false

  --EndScreen variables
  startScore = 0

  timeSpentMts = 0
  timeSpentSds = 0
  -- globalTimeMts = 0
  timeSds = 10
  timeMts = 0
  timeZero = ""

  -- globalTimeZero = 0

  LooseMessage = ""

  for _,v in pairs(Humans) do
    --change state to true (default false)
    v:changeState()
    v:changeAngle()

    
  end

  
  p:changeAngle()

  startScore = p.score



end

function generateLight()


  --Daylight
  if globalTimeHrs >= 10 and globalTimeHrs < 18 then
    ambientR = 245
    ambientG = 245
    ambientB = 245
    timeOfDay = "Day"
    --Evening
  elseif globalTimeHrs >= 18 and globalTimeHrs < 21 then
    ambientR = 170
    ambientG = 155
    ambientB = 155
    timeOfDay = "Evening"
    --Nighttime
  elseif globalTimeHrs >= 21 or globalTimeHrs < 5 then
    ambientR = 40
    ambientG = 40
    ambientB = 40
    timeOfDay = "Night"
    --Early Morning
  elseif globalTimeHrs >= 5 and globalTimeHrs < 10 then
    ambientR = 150
    ambientG = 150
    ambientB = 150
    timeOfDay = "Morning"
  end

  if timeOfDay ~= "Day" and levelEpoch == "Stone age" then
    lightMouseStrength = 150
  else
    lightMouseStrength = 0
  end




  lightWorld = LightWorld({

    ambient = {ambientR,ambientG,ambientB},
    refractionStrength = 32.0,
    reflectionVisibility = 0.75,
    --the general ambient light in the environment
  })

  if #campfireArray ~= 0 and (timeOfDay ~= "Day" and timeOfDay ~= "Morning") then
    for i = 0, #campfireArray, 5 do
      campfireArray[i+3] = nil
      campfireArray[i+3] = lightWorld:newLight(campfireArray[i+1], campfireArray[i+2], 200, math.random(140, 150), 100, math.random(200, 300))
    end

    lightWorld:newLight(ticketWindowX + 72, ticketWindowY + 123, 200, math.random(140, 150), 100, math.random(200, 300))
  end

  for _,v in pairs(Humans) do
    --change state to true (default false)


    table.insert(humanLights, lightWorld:newCircle(v.x + 16, v.y + 16, 16))
  end
  playerLight = lightWorld:newCircle(p.x + 16, p.y + 16, 16)

  lightMouse = lightWorld:newLight(0, 0, 200, 200, 100, lightMouseStrength)
  lightMouse:setGlowSize(0)

end

function checkMouseHover()
  local spriteOffset = 464
  isTooltipShowing = false

  for k, v in pairs(Humans) do
    if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      v.cx, v.cy, 32, 32) and not inventoryOpen then
      isTooltipShowing = true
      tooltipTimer = tooltipTimer + love.timer.getDelta()
      if tooltipTimer >= .3 then
        love.graphics.setColor(100, 100, 100, 150)
        love.graphics.rectangle("fill", love.mouse.getX(), love.mouse.getY(), 350, 200)
        love.graphics.setColor(255, 255, 255)
        love.graphics.print(v:tooltip(), love.mouse.getX() + 50, love.mouse.getY())

        --\/\/\/ Enable downfollowing code for extra debug purposes \/\/\/

        -- local i = 0
        -- for a, b in pairs(v) do
        --   love.graphics.print(a .. ":     " .. tostring(b), love.mouse.getX() + 50, love.mouse.getY() + i)
        --   i = i + 17
        -- end
      end
    end
  end

  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,       p.cx, p.cy, 32, 32) then

    isTooltipShowing = true
    tooltipTimer = tooltipTimer + love.timer.getDelta()
    if tooltipTimer >= .3 then
      love.graphics.setColor(100, 100, 100, 150)
      love.graphics.rectangle("fill", love.mouse.getX(), love.mouse.getY(), 350, 200)
      love.graphics.setColor(255, 255, 255)
      love.graphics.print(tostring(p), love.mouse.getX() + 50, love.mouse.getY())
    end
  end

  for k, v in pairs(skills) do
    if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      spriteOffset, love.graphics.getHeight() - 63, 62, 62) then

      isTooltipShowing = true
      tooltipTimer = tooltipTimer + love.timer.getDelta()
      if tooltipTimer >= 1 then
        love.graphics.setColor(100, 100, 100, 150)
        love.graphics.rectangle("fill", love.mouse.getX(), love.mouse.getY(), mainFont:getWidth(skillsTooltips[k]) + 10, - 75)
        love.graphics.setColor(255, 255, 255)
        love.graphics.print(skillsTooltips[k], love.mouse.getX(), love.mouse.getY() - 75)
      end

    end
    spriteOffset = spriteOffset + 72
  end


  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      1052, 656, 62, 62) then

    showUITooltip("Inventory.")

  end

  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      1124, 656, 62, 62) then


    showUITooltip("Unique skill.\n Will be added in the future.")
  end
  -- main time
  -- love.graphics.setColor(0, 255, 255)
  -- love.graphics.rectangle("line", 2, 655, 104, 30)
  -- meal time
  -- love.graphics.setColor(0, 255, 0)
  -- love.graphics.rectangle("line", 2, 685, 104, 30)
  -- boxes
  -- love.graphics.setColor(255, 255, 0)
  -- love.graphics.rectangle("line", 106, 655, 114, 30)
  -- close time
  -- love.graphics.setColor(255, 0, 0)
  -- love.graphics.rectangle("line", 106, 685, 114, 30)
  -- money
  -- love.graphics.setColor(0, 0, 255)
  -- love.graphics.rectangle("line", 235, 655, 210, 30)
  -- skills
  -- love.graphics.setColor(255, 0, 255)
  -- love.graphics.rectangle("line", 235, 685, 210, 30)

  --UI tooltips
  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      2, 655, 104, 30) then
    showUITooltip("Current time.")
  end
  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      2, 685, 104, 30) then
    showUITooltip("Meal break time of queue.")
  end
  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      106, 655, 114, 30) then
    if map.warehouseItemsCount > (map.allWarehouseItems/3*2) then
      showUITooltip("Lot of items left in the stock.")
    elseif map.warehouseItemsCount < (map.allWarehouseItems/3*2) and map.warehouseItemsCount > (map.allWarehouseItems/3) then
      showUITooltip("There are some items in the stock.")
    elseif map.warehouseItemsCount < (map.allWarehouseItems/3) then
      showUITooltip("Items in the stock almost ran out! Hurry!")
    end
  end
  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      106, 685, 114, 30) then
    showUITooltip("Time, when queue closes.")
  end
  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      235, 655, 210, 30) then
    showUITooltip("Your current money.")
  end
  if CheckCollision(love.mouse.getX(), love.mouse.getY(), 1, 1,      235, 685, 210, 30) then
    showUITooltip("Your available skill points.")
  end
end

function showUITooltip(message, delay)
  if delay == nil then
    delay = 1
  end

  isTooltipShowing = true
  tooltipTimer = tooltipTimer + love.timer.getDelta()
  if tooltipTimer >= delay then
    if love.mouse.getX() + mainFont:getWidth(message) + 10 < 1279 then
      love.graphics.setColor(100, 100, 100, 150)
      love.graphics.rectangle("fill", love.mouse.getX(), love.mouse.getY(), mainFont:getWidth(message) + 10, - 75)
      love.graphics.setColor(255, 255, 255)
      love.graphics.print(message, love.mouse.getX(), love.mouse.getY() - 75)
    else
      love.graphics.setColor(100, 100, 100, 150)
      love.graphics.rectangle("fill", love.mouse.getX() - (mainFont:getWidth(message) + 10), love.mouse.getY(), mainFont:getWidth(message) + 10, - 75)
      love.graphics.setColor(255, 255, 255)
      love.graphics.print(message, love.mouse.getX() - mainFont:getWidth(message) - 10, love.mouse.getY() - 75)
    end
  end
end

function assignTooltips()

  skillsTooltips = {}
  skillsTooltips[1] = " Rob \n Try to steal items or money from front-facing human.\n Success depends on luck and weared items."
  skillsTooltips[2] = " Persuade \n Try to tell front-facing human something, that makes him leave.\n Success depends on luck and weared items"
  skillsTooltips[3] = " Fight \n Try to start a fight."
  skillsTooltips[4] = " NONE \n Will be added in future updates. Support us on greenlight!"
  skillsTooltips[5] = " NONE \n Will be added in future updates. Support us on greenlight!"
  skillsTooltips[6] = " NONE \n Will be added in future updates. Support us on greenlight!"
  skillsTooltips[7] = " NONE \n Will be added in future updates. Support us on greenlight!"
  skillsTooltips[8] = " NONE \n Will be added in future updates. Support us on greenlight!"

end

function generateInventory()

  inv = Inventory:new()
  for i = 1, 8 do
    items[i] = Item:new(i)
    items[i]:generate()
  end


end