require "/states/game/MapGenerator/Cell"
fireImage = love.graphics.newImage('/images/sprites/fire_sheet.png')

MapGen = {}
junkArray = {}
borderArray = {}
campfireArray = {}

fireGrid = anim8.newGrid(76, 76, fireImage:getWidth(), fireImage:getHeight())
fireAnim = anim8.newAnimation(fireGrid('1-5', 1), 0.1)

ticketWindowX = nil
ticketWindowY = nil

function MapGen:new(_Name, length, epoch, diff)
  local obj = {Name = _Name}
  setmetatable(obj, self)
  self.__index = self
  obj.length = length
  obj.epoch = epoch
  obj.difficulty = diff
  obj.BlockSize = 34
  obj.startX = love.math.random(obj.BlockSize, obj.BlockSize * 3)
  obj.startY = love.math.random(200, love.graphics.getHeight() - obj.BlockSize - 200)
  obj.bezArray = {last = 0}
  obj.gameBG = nil
  obj.warehouseItemsCount = nil
  obj.allWarehouseItems = nil
  obj.lastBlockX = nil
  obj.lastBlockY = nil



  return obj
end

function MapGen:update(dt)
  fireAnim:update(dt)
end


function MapGen:generate()

  local CellsMap = {}
  

  if self.epoch == "Stone age" then
    self.gameBG = love.graphics.newImage("/images/levels/1/background_native.png")
    ticketWindow = love.graphics.newImage("/images/levels/1/trade_room.png")
    junkSpriteArray = love.filesystem.getDirectoryItems("/images/levels/1/sprites/")
    borderSpriteArray = love.filesystem.getDirectoryItems("/images/levels/1/sprites_border/")
  elseif self.epoch == "Antique" then
    --gameBG = love.graphics.newImage("/images/...")
  elseif self.epoch == "Middle ages" then
    --gameBG = love.graphics.newImage("/images/...")
  elseif self.epoch == "Renaissance" then
    --gameBG = love.graphics.newImage("/images/...")
  elseif self.epoch == "Industrial revolution" then
    --gameBG = love.graphics.newImage("/images/...")
  elseif self.epoch == "Soviet" then
    --gameBG = love.graphics.newImage("/images/...")
  end

  if self.difficulty == "Easy" then

  elseif self.difficulty == "Normal" then

  elseif self.difficulty == "Hard" then

  end

  if self.length == "Small" then
    self.length = math.random(6, 12)
    self.warehouseItemsCount = self.length - math.random(4)
    junkCount = math.random(4, 8)
    campfireCount = math.random(0,1)
    -- campfireCount = 0
  elseif self.length == "Medium" then
    self.length = math.random(12, 24)
    self.warehouseItemsCount = self.length - math.random(8)
    junkCount = math.random(8, 12)
    campfireCount = math.random(2,3)
  elseif self.length == "Large" then
    self.length = math.random(24, 36)
    self.warehouseItemsCount = self.length - math.random(12)
    junkCount = math.random(12, 16)
    campfireCount = math.random(3,4)
  end
  borderCount = self.length + math.random(-2, 4)

  self.allWarehouseItems = self.warehouseItemsCount
  levelSize = self.length
  levelSizeString = self.length

  
  for i = 0, junkCount*3, 3 do
    junkArray[i] = love.graphics.newImage("/images/levels/1/sprites/" .. junkSpriteArray[love.math.random(#junkSpriteArray)])
    -- X
    junkArray[i+1] = love.math.random(50, 1200)
    -- Y
    junkArray[i+2] = love.math.random(100, 600) 
  end



  if self.Name == "random" then
    
    local curX = self.startX
    local curY = self.startY

    for i = 1, self.length do
      CellsMap[i] = Cell:new(curX, curY, i)
      bezArray = pushlast(self.bezArray, curX + self.BlockSize/2)
      bezArray = pushlast(self.bezArray, curY + self.BlockSize/2)
      curX = curX + love.math.random(self.BlockSize, self.BlockSize * 3)
      curY = love.math.random(0, love.graphics.getHeight() - self.BlockSize)
    end

    

  elseif self.Name == "smooth" then
    local curX = self.startX
    local curY = self.startY

    for i = 1, self.length do

      CellsMap[i] = Cell:new(curX, curY, i)

      bezArray = pushlast(self.bezArray, curX + self.BlockSize/2 - 10)
      bezArray = pushlast(self.bezArray, curY + self.BlockSize/2)

      bezArray = pushlast(self.bezArray, curX + self.BlockSize/2)
      bezArray = pushlast(self.bezArray, curY + self.BlockSize/2)

      bezArray = pushlast(self.bezArray, curX + self.BlockSize/2 + 10)
      bezArray = pushlast(self.bezArray, curY + self.BlockSize/2)


      curX = curX + self.BlockSize + 10
      if curY >= 150 and curY <= love.graphics.getHeight() - self.BlockSize - 300 then
        curY = curY + love.math.random(-75, 75)
      elseif curY <= 150 then
        curY = curY + love.math.random(10, 75)
      elseif curY >= love.graphics.getHeight() - self.BlockSize - 300 then
        curY = curY + love.math.random(-10, -75)
      end
      if i == self.length - 1 then
        self.lastBlockX = curX
        self.lastBlockY = curY
      end
    end
  end

  for i = 0, borderCount*4, 4 do
    borderArray[i] = love.graphics.newImage("/images/levels/1/sprites_border/" .. borderSpriteArray[love.math.random(#borderSpriteArray)])
    local cellId = CellsMap[love.math.random(#CellsMap)]

    local xMod = 0

    if love.math.random(1,2) == 1 then
      yMod = love.math.random(70, 100)
    else
      yMod = love.math.random(-30, -50)
    end
    -- X
    borderArray[i+1] = cellId.x + love.math.random(-40, 40)
    -- Y
    borderArray[i+2] = cellId.y + yMod
    -- Rotation
    borderArray[i+3] = math.rad(love.math.random(360))
  end


  ticketWindowX = self.lastBlockX + 30
  ticketWindowY = self.lastBlockY - 5



  for i = 0, campfireCount * 5, 5 do
    local cellId = CellsMap[love.math.random(#CellsMap)]

    campfireArray[i] = love.graphics.newImage("/images/sprites/fireplace.png")
 

    -- X coordinate
    campfireArray[i+1] = love.math.random(-150, 150) + cellId.x
    -- Y coordinate
    if cellId.y > 200 and cellId.y < 600 then
      if math.random(1,2) == 1 then
        campfireArray[i+2] = love.math.random(-150, -250)
      else
        campfireArray[i+2] = love.math.random(150, 250)
      end
    elseif cellId.y > 600 then
      campfireArray[i+2] = love.math.random(-150, -500)
    elseif cellId.y < 200 then
      campfireArray[i+2] = love.math.random(150, 500)
    end
    campfireArray[i+2] = campfireArray[i+2] + cellId.y
    campfireArray[i+4] = math.random(0, 360)
  end
  return CellsMap
end

function MapGen:draw()
  
  

  love.graphics.draw(self.gameBG, 0, 0, 0, 1, 1)

  love.graphics.draw(ticketWindow, ticketWindowX, ticketWindowY, math.rad(270), 1, 1, 170, 0)
  


  local bezier = love.math.newBezierCurve(self.bezArray)
  love.graphics.setColor(180, 140, 100)
  love.graphics.setLineWidth(40)
  love.graphics.setLineJoin("bevel")
  love.graphics.line(bezier:render(1))
  love.graphics.setLineWidth(1)

  love.graphics.setColor(255, 255, 255)

  for i = 0, #junkArray, 3 do
    love.graphics.draw(junkArray[i], junkArray[i+1], junkArray[i+2]) 
  end
  for i = 0, #borderArray, 4 do
    love.graphics.draw(borderArray[i], borderArray[i+1], borderArray[i+2], borderArray[i+3], 1, 1, borderArray[i]:getWidth() / 2, borderArray[i]:getHeight() / 2) 
  end

  
    for i = 0, #campfireArray, 5 do
      love.graphics.draw(campfireArray[i], campfireArray[i+1], campfireArray[i+2], math.rad(campfireArray[i+4]), 0.5, 0.5, campfireArray[i]:getWidth()/2, campfireArray[i]:getHeight()/2)

      if timeOfDay ~= "Day" and timeOfDay ~= "Morning" then
        fireAnim:draw(fireImage, campfireArray[i+1], campfireArray[i+2], math.rad(campfireArray[i+4] + 75), 0.5, 0.5, 38, 38)
        fireAnim:draw(fireImage, ticketWindowX + 72, ticketWindowY + 123, math.rad(148), 0.5, 0.5, 38, 38)
      end
    end




  

  love.graphics.setColor(255, 255, 255)
end


function pushlast(list, value)
  local last = list.last + 1
  list.last = last
  list[last] = value
  return list
end
