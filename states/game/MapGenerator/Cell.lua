Cell = {}

function Cell:new(curX, curY, id)
  local obj = {}
  setmetatable(obj, self)
  self.__index = self
  obj.x = curX
  obj.y = curY
  obj.id = id
  obj.size = 17
  return obj
end

function Cell:draw()
   love.graphics.setColor(100,100,100, 100)
   love.graphics.circle("line", self.x + self.size, self.y + self.size, self.size)
end

function Cell:__tostring()
  return "id: "..self.id.."\n\rx: "..self.x.."\n\ry: "..self.y.."\n\rsize: "..self.size.."\n\r------\n\r"
end
