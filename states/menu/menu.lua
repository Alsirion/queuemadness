menu = {}


ColorTextRed = {178, 18, 59}
ColorTextBlue = {11, 145, 190}




local LightWorld = require "lib"
require("/states/game/Player")
require("/states/menu/train")
suit = require 'suit'
anim8 = require 'anim8/anim8'
mainFont = love.graphics.newFont("/fonts/Red October-Regular.ttf", 20);


HeroImage = love.graphics.newImage('/images/player/player_sheet.png')
local heroGrid = anim8.newGrid(48, 48, HeroImage:getWidth(), HeroImage:getHeight())
local heroAnim = anim8.newAnimation(heroGrid('1-26',1), 0.02)

trainX = -1000

gameDifficulty = "Hard"



local player
-- Check overlapping snippet from wiki
-- To be clear, I'm absolutely dont understand how it works, but it works
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
-- End of snippet










function menu:init()

	trainSys = Train:new(1)
	trainSys:generate()

	love.graphics.setFont(mainFont)
	bg = love.graphics.newImage("/images/menu/menu.png")

		playerX = love.graphics.getWidth()/2 - 16
		playerY = love.graphics.getHeight()/2 - 16
		playerAngle = 0

end

function menu:enter(previous)

	love.physics.setMeter(64)
	physicsWorld = love.physics.newWorld(0, 0, true)





	playerSpeed = 450
	-- if previous == level_sel then
	-- 	playerAngle = 180
	-- 	-- playerX = love.graphics.getWidth()/2 - 16
	-- 	-- playerY = love.graphics.getHeight()/4*2 - 16
	-- else
	-- 	playerAngle = 0
	-- 	playerX = love.graphics.getWidth()/2 - 16
	-- 	playerY = love.graphics.getHeight()/2 - 16
	-- end


	physicsObjects = {}


	physicsObjects.player = {}
	physicsObjects.player.body = love.physics.newBody(physicsWorld, playerX - 24, playerY - 24, "dynamic")
	physicsObjects.player.shape = love.physics.newRectangleShape(48, 48)
	physicsObjects.player.fixture = love.physics.newFixture(physicsObjects.player.body, physicsObjects.player.shape)


	physicsObjects.column1 = {}
	physicsObjects.column1.body = love.physics.newBody(physicsWorld, 110, 307)
	physicsObjects.column1.shape = love.physics.newCircleShape(34)
	physicsObjects.column1.fixture = love.physics.newFixture(physicsObjects.column1.body, physicsObjects.column1.shape)

	physicsObjects.column2 = {}
	physicsObjects.column2.body = love.physics.newBody(physicsWorld, 455, 307)
	physicsObjects.column2.shape = love.physics.newCircleShape(34)
	physicsObjects.column2.fixture = love.physics.newFixture(physicsObjects.column2.body, physicsObjects.column2.shape)

	physicsObjects.column3 = {}
	physicsObjects.column3.body = love.physics.newBody(physicsWorld, 825, 307)
	physicsObjects.column3.shape = love.physics.newCircleShape(34)
	physicsObjects.column3.fixture = love.physics.newFixture(physicsObjects.column3.body, physicsObjects.column3.shape)

	physicsObjects.column4 = {}
	physicsObjects.column4.body = love.physics.newBody(physicsWorld, 1170, 307)
	physicsObjects.column4.shape = love.physics.newCircleShape(34)
	physicsObjects.column4.fixture = love.physics.newFixture(physicsObjects.column4.body, physicsObjects.column4.shape)

	physicsObjects.locomotive = {}
	physicsObjects.locomotive.body = love.physics.newBody(physicsWorld, trainX + 650 + locomotive:getWidth()/2, 19 + 93.5)
	physicsObjects.locomotive.shape = love.physics.newRectangleShape(locomotive:getWidth(), 187)
	physicsObjects.locomotive.fixture = love.physics.newFixture(physicsObjects.locomotive.body, physicsObjects.locomotive.shape)

	physicsObjects.border = {}
	physicsObjects.border.body = love.physics.newBody(physicsWorld, 0, 0)
	physicsObjects.border.shape = love.physics.newChainShape(true, 0,0, 0,666, 1280,666, 1280,0)
	physicsObjects.border.fixture = love.physics.newFixture(physicsObjects.border.body, physicsObjects.border.shape)






	lightWorld = LightWorld({
	  ambient = {55,55,55},
	  refractionStrength = 1.0,
	  reflectionVisibility = 0.75,
	           --the general ambient light in the environment
	})

	--Light Fuckups starts here

	lightMouse = lightWorld:newLight(0, 0, 100, 200, 200, 300)
	lightMouse:setGlowStrength(0.3)
	
	-- humanShadow = lightWorld:newCircle(playerX, playerY, 12)
	-- humanShadow:setPosition(playerX, playerY, 2)

	stationLight_l = lightWorld:newLight(270, 480, 230, 230, 230, 500)
	stationLight_l:setGlowStrength(0.3)
	-- stationLight_l:setPosition(270, 480, 5)

	stationLight_2 = lightWorld:newLight(950, 480, 230, 230, 230, 500)
	stationLight_2:setGlowStrength(0.3)

	column1 = lightWorld:newCircle(110, 307, 34)
	column2 = lightWorld:newCircle(455, 307, 34)
	column3 = lightWorld:newCircle(825, 307, 34)
	column4 = lightWorld:newCircle(1170, 307, 34)
	-- column1:setPosition(110, 307, 34)
	-- column2:setPosition(455, 307, 34)
	-- column3:setPosition(825, 307, 34)
	-- column4:setPosition(1170, 307, 34)

	-- trainShadow = lightWorld:newRectangle(-1000, -1000, love.graphics.getWidth() - 70, 200)

	trainHeadlight_upper = lightWorld:newLight(-1000, 115, 230, 230, 100, 400)
	trainHeadlight_upper:setGlowStrength(2)
	trainHeadlight_upper:setAngle(1.8)
	trainLopLight = lightWorld:newLight(-1000, 115, 50, 230, 230, 200)
	trainLopLight:setGlowStrength(.3)



	for k,v in pairs(vagons) do
		vagonLights[k] = lightWorld:newLight(v.x + 272, v.y + 90, 230, 230, 100, 300)
		vagonLights[k]:setGlowStrength(0.7)
		vagonShadowsCache = {
			lightWorld:newRectangle(v.x + 9, v.y + 1,8,180),

			lightWorld:newRectangle(v.x + 17, v.y + 1,24,8),

			lightWorld:newRectangle(v.x + 17, v.y + 173,24,8),

			lightWorld:newRectangle(v.x + 127, v.y + 1,26,8),

			lightWorld:newRectangle(v.x + 127, v.y + 173,26,8),

			lightWorld:newRectangle(v.x + 239, v.y + 1,84,8),

			lightWorld:newRectangle(v.x + 409, v.y + 1,26,8),

			lightWorld:newRectangle(v.x + 521, v.y + 1,24,8),

			lightWorld:newRectangle(v.x + 521, v.y + 173,24,8),

			lightWorld:newRectangle(v.x + 545, v.y + 1,8,180),

			lightWorld:newRectangle(v.x + 409, v.y + 173,26,8),

			lightWorld:newRectangle(v.x + 239, v.y + 173,16,8),

			lightWorld:newRectangle(v.x + 409, v.y + 173,16,8)
		}
		for k, v in pairs(vagonShadowsCache) do
			table.insert(vagonShadows, v)
		end
	end

	gameDifficulty = "Hard"
end

function menu:update(dt)
	heroAnim:update(dt)
	heroAnim:pause()

	physicsWorld:update(dt)

	trainSys:update(dt)


	if love.keyboard.isDown("w") then		
		-- playerY = playerY - playerSpeed * dt

		physicsObjects.player.body:applyForce(0, -playerSpeed * 30)
		heroAnim:resume()
		playerAngle = 0
	end

	if love.keyboard.isDown("s") then		
		-- playerY = playerY + playerSpeed * dt

		physicsObjects.player.body:applyForce(0, playerSpeed * 30)
		heroAnim:resume()
		playerAngle = 180
	end

	if love.keyboard.isDown("d") then
		-- playerX = playerX + playerSpeed * dt

		physicsObjects.player.body:applyForce(playerSpeed * 30, 0)
		heroAnim:resume()
		playerAngle = 90
	end

	if love.keyboard.isDown("a") then
		-- playerX = playerX - playerSpeed * dt

		physicsObjects.player.body:applyForce(-playerSpeed * 30, 0)
		heroAnim:resume()
		playerAngle = 270
	end

	playerX = physicsObjects.player.body:getX()
	playerY = physicsObjects.player.body:getY()
	physicsObjects.player.body:setLinearVelocity(0, 0)
	-- physicsObjects.player.body:setPosition(playerX, playerY)

	if love.keyboard.isDown("w") and love.keyboard.isDown("d") then
		playerAngle = 45
	end
	if love.keyboard.isDown("w") and love.keyboard.isDown("a") then
		playerAngle = 315
	end
	if love.keyboard.isDown("s") and love.keyboard.isDown("d") then
		playerAngle = 135
	end
	if love.keyboard.isDown("s") and love.keyboard.isDown("a") then
		playerAngle = 225
	end

	
	-- self.map_collide()
	-- trainSpeed = (15000 - trainX - 650)/100

	-- if trainX < 125 then
	-- 	trainX = trainX + trainSpeed
	-- end

	physicsObjects.locomotive.body:setPosition(trainX + 650 + locomotive:getWidth()/2, 19 + 93.5)





	-- if CheckCollision(playerX, playerY, 32, 32, trainX - locomotive:getWidth() + 25, 25, love.graphics.getWidth() - 70, 175) then
	-- 	trainEntered = true
	-- else 
	-- 	trainEntered = false
	-- end

	if CheckCollision(playerX, playerY, 32, 32, love.graphics.getWidth() - 160, 550, 150, 200) then
		settingsEntered = true
	else 
		settingsEntered = false
	end

	if CheckCollision(playerX, playerY, 32, 32, love.graphics.getWidth()/2 - 75, love.graphics.getHeight() - 100, 150, 100) then
		exitEntered = true
	else 
		exitEntered = false
	end

	lightMouse:setPosition(love.mouse.getX(), love.mouse.getY())
	for k,v in pairs(vagons) do
		for a,b in pairs(vagonLights) do
			if a == k then
				v:setPosition(v.x + 272, v.y + 90)
			end
		end
		-- for k,v in pairs(vagonShadows) do
			

		-- end
		for i = 0, #vagonShadows, 12 do

			vagonShadows[i]:setPosition(v.x + 9, v.y + 1)
			vagonShadows[i+1]:setPosition(v.x + 17, v.y + 1)
			vagonShadows[i+2]:setPosition(v.x + 17, v.y + 173)
			vagonShadows[i+3]:setPosition(v.x + 127, v.y + 1)
			vagonShadows[i+4]:setPosition(v.x + 127, v.y + 173)
			vagonShadows[i+5]:setPosition(v.x + 239, v.y + 1)
			vagonShadows[i+6]:setPosition(v.x + 409, v.y + 1)
			vagonShadows[i+7]:setPosition(v.x + 521, v.y + 1)
			vagonShadows[i+8]:setPosition(v.x + 521, v.y + 173)
			vagonShadows[i+9]:setPosition(v.x + 545, v.y + 1)
			vagonShadows[i+10]:setPosition(v.x + 409, v.y + 173)
			vagonShadows[i+11]:setPosition(v.x + 239, v.y + 173)
			vagonShadows[i+12]:setPosition(v.x + 409, v.y + 173)
		end
	end
	trainHeadlight_upper:setPosition(trainX + 1090, 115)
	trainLopLight:setPosition(trainX + 675, 115)

	-- if math.random(200) <= 1 then
	-- 	stationLight_l.visible = not stationLight_l.visible
	-- end

	lightWorld:update(dt)
	
end

function menu:draw()
	lightWorld:draw(function()
		-- love.graphics.setColor(0,0,0)
		love.graphics.setColor(255,255,255)
		love.graphics.draw(bg, 0, 0, 0, 1, 1)
		love.graphics.setBackgroundColor(255,255,255)
		
		-- love.graphics.setColor(100, 200, 100)


		
		--Train
		trainSys:draw()
		
		--Settings
		love.graphics.setColor(200, 150, 150, 100)
		love.graphics.rectangle("fill", love.graphics.getWidth() - 160, 550, 150, 200)
		love.graphics.setColor(100, 200, 100)
		--Exit
		love.graphics.setColor(150, 150, 200, 100)
		love.graphics.rectangle("fill", love.graphics.getWidth()/2 - 75, love.graphics.getHeight() - 100, 150, 100)
		love.graphics.setColor(100, 200, 100)

		love.graphics.setColor(255,255,255)
		heroAnim:draw(HeroImage, playerX, playerY, math.rad(playerAngle), 1, 1, 24, 24)
		love.graphics.setColor(100, 200, 100)

		-- love.graphics.rectangle("line", playerX - 24, playerY - 24, 48, 48)	

		


	end)

	love.graphics.setBackgroundColor(50, 50, 50)
	trainSys:drawRoofs()
	love.graphics.setBackgroundColor(255, 255, 255)

	local x = love.mouse.getX()
	local y = love.mouse.getY()
	-- love.graphics.print(x .." ".. y)

	if exitEntered then
		love.graphics.print("Quit? Y N", 10, 10)
		if love.keyboard.isDown("y") then
			love.event.quit()
		end
	end
	love.graphics.setColor(ColorTextRed)
	love.graphics.print("WASD to move \n Y to enter", 30, 300)
	love.graphics.setColor(255, 255, 255)

	-- if trainEntered then
		
	-- end
	
	if settingsEntered then
		love.graphics.print("Enter to Settings? Y N", 10, 10)
		if love.keyboard.isDown("y") then
			settingsEnabled = true
		end
	end
	if settingsEnabled then
		if love.keyboard.isDown("escape") then
			settingsEnabled = not settingsEnabled
		end
		love.graphics.setColor(0, 0, 0, 180)
		love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
		love.graphics.setColor(100, 100, 100)
		love.graphics.rectangle("fill", 200, 100, love.graphics.getWidth() - 400, love.graphics.getHeight() - 200)
		love.graphics.setColor(255, 255, 255)
		love.graphics.print("Place for cozy settings :)", love.graphics.getWidth()/2 - #("Place for cozy settings :)")*4, love.graphics.getHeight()/2)
		love.graphics.print("Press esc for quit", love.graphics.getWidth()/2 - #("Press esc for quit")*4, love.graphics.getHeight()/3*2)
	end

end

function menu:keypressed(key)

end


-- function menu:map_collide()
-- 	if playerX < 0 then
-- 		playerX = 0
-- 	end
-- 	if playerY < 0 then
-- 		playerY = 0
-- 	end
-- 	if playerX + 32 > love.graphics.getWidth() then
-- 		playerX = love.graphics.getWidth() - 32
-- 	end
-- 	if playerY > 650 then
-- 		playerY = 650
-- 	end
-- end
