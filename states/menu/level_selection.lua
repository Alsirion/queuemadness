level_sel = {}

suit = require 'suit'
local LightWorld = require "lib"
local bg = love.graphics.newImage("/images/menu/menu.png")

levels = {}

local timer = 0
globalTimeHrs = 18
globalTimeMts = 00
levelBreakHrs = 0
levelBreakMts = 0
globalTimeZero = 0
local player

levels = {}
completedLevels = {}
-- HeroImage = love.graphics.newImage('/images/player/nip_sheet.png')
local heroGrid = anim8.newGrid(48, 48, HeroImage:getWidth(), HeroImage:getHeight())
local heroAnim = anim8.newAnimation(heroGrid('1-26',1), 0.02)

-- Check overlapping snippet from wiki
-- To be clear, I'm absolutely dont understand how it works, but it works
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
-- End of snippet

function level_sel:init()

	levels.stoneAge = {}


	levels.stoneAge[1] = {

	id = 1,

	x = 365,
	y = 350,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 23,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Small",
	levelDifficulty = "Easy",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = "axe",
	isCompleted = false,
	rewardPrice = 100

	}

	levels.stoneAge[2] = {

	id = 2,

	x = 765,
	y = 350,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 22,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Small",
	levelDifficulty = "Easy",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = nil,
	isCompleted = false,
	rewardPrice = 100

	}
	
	levels.stoneAge[3] = {

	id = 3,

	x = 765,
	y = 550,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 22,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Medium",
	levelDifficulty = "Medium",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = nil,
	isCompleted = false,
	rewardPrice = 100

	}
	levels.stoneAge[3] = {

	id = 3,

	x = 765,
	y = 550,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 22,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Medium",
	levelDifficulty = "Medium",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = nil,
	isCompleted = false,
	rewardPrice = 100

	}
	levels.stoneAge[4] = {

	id = 4,

	x = 765,
	y = 550,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 22,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Medium",
	levelDifficulty = "Medium",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = nil,
	isCompleted = false,
	rewardPrice = 100

	}
	levels.stoneAge[5] = {

	id = 5,

	x = 765,
	y = 550,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 22,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Medium",
	levelDifficulty = "Medium",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = nil,
	isCompleted = false,
	rewardPrice = 100

	}
	levels.stoneAge[6] = {

	id = 6,

	x = 765,
	y = 550,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 22,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Medium",
	levelDifficulty = "Medium",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = nil,
	isCompleted = false,
	rewardPrice = 100

	}
	levels.stoneAge[7] = {

	id = 7,

	x = 765,
	y = 550,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 22,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Medium",
	levelDifficulty = "Medium",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = nil,
	isCompleted = false,
	rewardPrice = 100

	}
	levels.stoneAge[8] = {

	id = 8,

	x = 765,
	y = 550,
	w = 150,
	h = 100,

	r = love.math.random(0,255),
	g = love.math.random(0,255),
	b = love.math.random(0,255),

	levelWorkHrsStart = 10,
	levelWorkHrsFinish = 22,
	levelWorkMtsStart = 0,
	levelWorkMtsFinish = 0,

	levelBreakHrsStart = 13,
	levelBreakHrsFinish = 14,
	levelBreakMtsStart = 20,
	levelBreakMtsFinish = 30,

	levelEpoch = "Stone age",
	levelSizeString = "Medium",
	levelDifficulty = "Medium",
	levelRequirements = "None",
	levelEnemysDetected = "None",

	levelReward = nil,
	isCompleted = false,
	rewardPrice = 100

	}

end

function level_sel:enter()
	love.graphics.setFont(mainFont)

	-- globalTimeMts = globalTimeMts + game.timeSpent

	-- playerX = love.graphics.getWidth()/2 - 16
	-- playerY = 500 - 32

	for k, v in pairs(completedLevels) do
		for a, b in pairs(levels.stoneAge) do
			if b.id == v then
				b.isCompleted = true
			end
		end
	end

	lightWorld = LightWorld({
	  ambient = {55,55,55},
	  refractionStrength = 1.0,
	  reflectionVisibility = 0.75,
	           --the general ambient light in the environment
	})

	--Light Fuckups starts here

	lightMouse = lightWorld:newLight(0, 0, 100, 200, 200, 300)
	lightMouse:setGlowStrength(0.3)
	
	-- humanShadow = lightWorld:newCircle(playerX, playerY, 12)
	-- humanShadow:setPosition(playerX, playerY, 2)

	stationLight_l = lightWorld:newLight(270, 480, 230, 230, 230, 500)
	stationLight_l:setGlowStrength(0.3)
	-- stationLight_l:setPosition(270, 480, 5)

	stationLight_2 = lightWorld:newLight(950, 480, 230, 230, 230, 500)
	stationLight_2:setGlowStrength(0.3)

	column1 = lightWorld:newCircle(110, 307, 34)
	column2 = lightWorld:newCircle(455, 307, 34)
	column3 = lightWorld:newCircle(825, 307, 34)
	column4 = lightWorld:newCircle(1170, 307, 34)
	-- column1:setPosition(110, 307, 34)
	-- column2:setPosition(455, 307, 34)
	-- column3:setPosition(825, 307, 34)
	-- column4:setPosition(1170, 307, 34)

	-- trainShadow = lightWorld:newRectangle(-1000, -1000, love.graphics.getWidth() - 70, 200)

	trainHeadlight_upper = lightWorld:newLight(-1000, 115, 230, 230, 100, 400)
	trainHeadlight_upper:setGlowStrength(2)
	trainHeadlight_upper:setAngle(1.8)
	trainLopLight = lightWorld:newLight(-1000, 115, 50, 230, 230, 200)
	trainLopLight:setGlowStrength(.3)

	for k,v in pairs(vagons) do
		vagonLights[k] = lightWorld:newLight(v.x + 272, v.y + 90, 230, 230, 100, 300)
		vagonLights[k]:setGlowStrength(0.7)
	end

	-- if CheckCollision(playerX, playerY, 32, 32, 565, 350, 150, 100) then
	-- 	levelChosen = true


end

function level_sel:update(dt)


	lightWorld:update(dt)
	trainSys:update(dt)

	heroAnim:update(dt)
	heroAnim:pause()




	if love.keyboard.isDown("w") then		
		playerY = playerY - playerSpeed * dt
		heroAnim:resume()
		playerAngle = 0
	end

	if love.keyboard.isDown("s") then		
		playerY = playerY + playerSpeed * dt
		heroAnim:resume()
		playerAngle = 180
	end

	if love.keyboard.isDown("d") then
		heroAnim:resume()
		playerX = playerX + playerSpeed * dt
		playerAngle = 90
	end

	if love.keyboard.isDown("a") then
		heroAnim:resume()
		playerX = playerX - playerSpeed * dt
		playerAngle = 270
	end

	if love.keyboard.isDown("w") and love.keyboard.isDown("d") then
		playerAngle = 45
	end
	if love.keyboard.isDown("w") and love.keyboard.isDown("a") then
		playerAngle = 315
	end
	if love.keyboard.isDown("s") and love.keyboard.isDown("d") then
		playerAngle = 135
	end
	if love.keyboard.isDown("s") and love.keyboard.isDown("a") then
		playerAngle = 225
	end

	self.map_collide()

	timer = timer + love.timer.getDelta()

	if timer >= 1 then

		globalTimeMts = globalTimeMts + 1
		if globalTimeMts < 10 then
			globalTimeZero = 0
		else 
			globalTimeZero = ""
		end

		if globalTimeMts >= 60 then
			if globalTimeHrs < 23 then
				globalTimeHrs = globalTimeHrs + 1
			else
				globalTimeHrs = 0
			end
			globalTimeMts = 0
		end

	 	timer = 0
	end


	levelChosen = false

	levelWorkHrsStart = nil
	levelWorkHrsFinish = nil
	levelWorkMtsStart = nil
	levelWorkMtsFinish = nil

	levelBreakHrsStart = nil
	levelBreakHrsFinish = nil
	levelBreakMtsStart = nil
	levelBreakMtsFinish = nil

	levelID = nil
	levelEpoch = nil
	levelWorkTime = nil
	levelBreakTime = nil
	levelSizeString = nil
	levelDifficulty = nil
	levelRequirements = nil
	levelEnemysDetected = nil
	levelReward = nil
	levelPrice = nil
	levelIsCompleted = nil

	for k, v in pairs(levels.stoneAge) do

			if CheckCollision(playerX, playerY, 32, 32, v.x, v.y, v.w, v.h) then

				levelChosen = true

				levelWorkHrsStart = v.levelWorkHrsStart
				levelWorkHrsFinish = v.levelWorkHrsFinish
				levelWorkMtsStart = v.levelWorkMtsStart
				levelWorkMtsFinish = v.levelWorkMtsFinish

				levelBreakHrsStart = v.levelBreakHrsStart
				levelBreakHrsFinish = v.levelBreakHrsFinish
				levelBreakMtsStart = v.levelBreakMtsStart
				levelBreakMtsFinish = v.levelBreakMtsFinish

				levelID = v.id
				levelEpoch = v.levelEpoch
				levelWorkTime = "From ".. v.levelWorkHrsStart ..", till ".. v.levelWorkHrsFinish .." "
				levelBreakTime = "From ".. v.levelBreakHrsStart .. ":" .. v.levelBreakMtsStart .. ", till ".. v.levelBreakHrsFinish ..":" .. v.levelBreakMtsFinish
				levelSizeString = v.levelSizeString
				levelDifficulty = v.levelDifficulty
				levelRequirements = v.levelRequirements
				levelEnemysDetected = v.levelEnemysDetected
				levelReward = v.levelReward
				levelPrice = v.rewardPrice
				levelIsCompleted = v.isCompleted
			end

	end



	-- if CheckCollision(playerX, playerY, 32, 32, love.graphics.getWidth()/2 -75, 550, 150, 150) then
	-- 	exitEntered = true
	-- else 
	-- 	exitEntered = false
	-- end

	for a,b in pairs(vagons) do
		for k,v in pairs(vagonLights) do
			if a == k then
				v:setPosition(b.x + 272, b.y + 90)
			end
		end
	end

	lightMouse:setPosition(love.mouse.getX(), love.mouse.getY())

	trainHeadlight_upper:setPosition(trainX + 1090, 115)
	trainLopLight:setPosition(trainX + 675, 115)



end

function level_sel:draw()
	lightWorld:draw(function()

		love.graphics.setColor(100, 200, 100)
		local x = love.mouse.getX()
		local y = love.mouse.getY()

		love.graphics.print("Current time: " .. globalTimeHrs .. " : " .. globalTimeZero .. globalTimeMts, 50, 0)


		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(bg, 0, 0, 0, 1, 1)	
		-- love.graphics.setColor(100, 200, 100)
		trainSys:draw()

		-- love.graphics.setColor(150, 150, 200)
		-- love.graphics.rectangle("fill", 0 , 300, love.graphics.getWidth(), 500)
		-- love.graphics.setColor(100, 200, 100)

		-- love.graphics.setColor(250, 150, 150)
		-- love.graphics.rectangle("fill", love.graphics.getWidth()/2 -75, 350, 150, 100)
		-- love.graphics.setColor(200, 100, 100)

		for k, v in pairs(levels.stoneAge) do
			love.graphics.setColor(v.r, v.g, v.b, 100)
			love.graphics.rectangle("fill", v.x, v.y, v.w, v.h)
			love.graphics.setColor(200, 100, 100)
		end

		love.graphics.setColor(150, 150, 250)
		love.graphics.rectangle("fill", love.graphics.getWidth()/2 -75, 550, 150, 100)
		love.graphics.setColor(100, 200, 100)

		love.graphics.print("Epoch: ", love.graphics.getWidth()/2 - 300, 100)
		love.graphics.print("Work Hours: ", love.graphics.getWidth()/2 - 300, 120)
		love.graphics.print("Lunch Brake Hours: ", love.graphics.getWidth()/2 - 300, 140)
		love.graphics.print("Queue Length: ", love.graphics.getWidth()/2 - 300, 160)
		love.graphics.print("Queue Difficulty: ", love.graphics.getWidth()/2 - 300, 180)
		love.graphics.print("Special Requirements: ", love.graphics.getWidth()/2 - 300, 200)
		love.graphics.print("Enemies in queue: ", love.graphics.getWidth()/2 - 300, 220)
		love.graphics.print("Reward and price: ", love.graphics.getWidth()/2 - 300, 240)




		love.graphics.setColor(255,255,255)
		




		-- love.graphics.rectangle("fill", playerX, playerY, 32, 32)
		-- love.graphics.draw(HeroImage, playerX, playerY, 0, 1, 1)
		love.graphics.setColor(255,255,255)
		heroAnim:draw(HeroImage, playerX, playerY, math.rad(playerAngle), 1, 1, 24, 24)
		love.graphics.setColor(100, 200, 100)

	end)

	if levelChosen then

		love.graphics.print(levelEpoch, love.graphics.getWidth()/2, 100)
		love.graphics.print(levelWorkTime, love.graphics.getWidth()/2, 120)
		love.graphics.print(levelBreakTime, love.graphics.getWidth()/2, 140)
		love.graphics.print(levelSizeString, love.graphics.getWidth()/2, 160)
		love.graphics.print(levelDifficulty, love.graphics.getWidth()/2, 180)
		love.graphics.print(levelRequirements, love.graphics.getWidth()/2, 200)
		love.graphics.print(levelEnemysDetected, love.graphics.getWidth()/2, 220)
		love.graphics.print(tostring(levelReward) .. " - " .. levelPrice .. " rub", love.graphics.getWidth()/2, 240)

		if levelIsCompleted then
			love.graphics.print("You have already completed this level.", love.graphics.getWidth()/2 - #"It is inappropriate time." * 4 - 125, 270)
			enterPossible = false
		elseif globalTimeHrs >= levelWorkHrsStart and globalTimeHrs < levelWorkHrsFinish then
			love.graphics.print("Press space to play this level", love.graphics.getWidth()/2 - #"Press space to play this level" * 4 - 125, 270)
			enterPossible = true
		elseif not (globalTimeHrs >= levelWorkHrsStart and globalTimeHrs < levelWorkHrsFinish) then
			love.graphics.print("It is inappropriate time.", love.graphics.getWidth()/2 - #"It is inappropriate time." * 4 - 125, 270)
			enterPossible = false

		end

	end

	if exitEntered then
		Gamestate.switch(menu)
	end

	love.graphics.setBackgroundColor(255, 255, 255)

end

function level_sel:keypressed(key)
	if levelChosen and enterPossible and key == "space" then
		Gamestate.switch(game2)
	end

	if key == "z" then
		globalTimeMts = globalTimeMts + 10
	end

end


-- function player_move(dt)

-- 	if love.keyboard.isDown("w") then
-- 		playerY = playerY - playerSpeed * dt
-- 	end

-- 	if love.keyboard.isDown("s") then
-- 		playerY = playerY + playerSpeed * dt
-- 	end

-- 	if love.keyboard.isDown("d") then
-- 		playerX = playerX + playerSpeed * dt
-- 	end

-- 	if love.keyboard.isDown("a") then
-- 		playerX = playerX - playerSpeed * dt
-- 	end
-- end

function level_sel:map_collide()
	-- if playerX - 32 < 125 then
	-- 	playerX = 125 + 32
	-- end
	-- if playerY - 32 < 340 then
	-- 	playerY = 340 + 32
	-- end
	-- if playerX + 32 > love.graphics.getWidth() - 115 then
	-- 	playerX = love.graphics.getWidth() - 115 - 32
	-- end
	-- if playerY + 32 > 720 - 59 then
	-- 	playerY = 720 - 59 - 32
	-- end
end