

Train = {}

locomotive = love.graphics.newImage("/images/sprites/locomotive.png")
trainRoofTop = love.graphics.newImage("images/sprites/roof_shadowed.png")
trainInteriors = {}
trainInteriors[1] = love.graphics.newImage("/images/levels/1/train/train.png")
vagons = {}
vagonLights = {}
vagonShadows = {}

function Train:new(length)
  local obj = {}
  setmetatable(obj, self)
  self.__index = self
  obj.length = length
  obj.destinationX = 1450
  obj.destinationY = 25
  obj.X = -100
  obj.Y = 39
  -- obj.speed = 15000 / (obj.destinationX - obj.X)
  obj.speed = 50
  return obj
end

function Train:generate()

	for i = 1, self.length do

		vagons[i] = {
			x = self.X - locomotive:getWidth() - i * 560,
			y = 21,
			interior = trainInteriors[i],
			roof = trainRoofTop,
			-- light = lightWorld:newLight(-1000, 0, 100, 200, 200, 300)
		}
	end
end

function Train:draw()
	love.graphics.draw(locomotive, self.X - locomotive:getWidth(), self.Y)
	for k, v in pairs(vagons) do
		love.graphics.draw(v.interior, v.x, v.y)

		love.graphics.print(Gamestate.current())
		-- love.graphics.rectangle("fill", v.x, v.y + 250, 555, 48)

		if CheckCollision(playerX + 16, playerY + 16, 1, 1, v.x, v.y + 250, 555, 48) and Gamestate.current() == level_sel then
			Gamestate.switch(menu)
	 		physicsObjects.player.body:setX(playerX)
	 		physicsObjects.player.body:setY(playerY)
			
		end
	end

end

function Train:drawRoofs()
	for k, v in pairs(vagons) do
		love.graphics.draw(v.roof, v.x, v.y)

		-- love.graphics.rectangle("fill", v.x + 245, v.y + 160, 55, 48)
		if CheckCollision(playerX + 16, playerY + 16, 1, 1, v.x + 245, v.y + 160, 55, 48) then
			Gamestate.switch(level_sel)
		end
	end

	love.graphics.print(self.X)
end

function Train:update(dt)
	if self.X < self.destinationX then
		self.X = self.X + self.speed
		for k, v in pairs(vagons) do
			v.x = self.X - locomotive:getWidth() - k * 560
		end
	end
	-- for k, v in pairs(vagons) do
	-- 	if CheckCollision(playerX + 16, playerY + 16, 1, 1, v.x + 245, v.y + 160, 55, 96)then
	-- 		-- love.graphics.rectangle("fill",v.x + 245, v.y + 112, 55, 96)
	-- 		if Gamestate.current() == level_sel then
	-- 			-- Gamestate.switch(menu)
	-- 			-- playerY = playerY - 135
	-- 		elseif Gamestate.current() == menu then
	-- 			Gamestate.switch(level_sel)
	-- 			playerY = playerY - 135
	-- 		end
	-- 	end
	-- end
end